@extends('layouts.admin_master')
@section('main_content')
@section('title', 'Activity Full Details')
        {!! Html::script('assets/js/javascript/globalize.js', array(), true) !!}
        {!! Html::script('assets/js/javascript/jqx-all.js', array(), true) !!}
        {!! Html::script('assets/js/javascript/date.js', array(), true) !!}
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=9o7h1hmv1r0qpuakt1dbrwbsfb4h0o72u8ed1ji6rrhy1a14"></script>
<script>tinymce.init({ selector:'textarea#itinerary'});</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
     <script src='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.js'></script>
<link rel='stylesheet' href='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.css' />


<link href="{{url('assets/css/js-image-slider.css', array(), true)}}" rel="stylesheet" type="text/css" />
    <script src="{{url('assets/js/js-image-slider.js', array(), true)}}" type="text/javascript"></script>

<div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Activity Details</h4>
          </div>

          <!-- dialog body -->
          <div class="modal-body" id ='modal-body'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Hello world!
          </div>
          <!-- dialog buttons -->
          <!-- <div class="modal-footer"><button type="button" class="btn btn-primary">OK</button></div> -->
        </div>
      </div>
    </div>

<div class="modal" id="add_activity_photo_modal" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Change</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
        <form id ="upload-activity-photo" method='post'  enctype="multipart/form-data"  action ="{{url('motherboard/activity/photo', array(), true)}}">
             {!! csrf_field() !!}
            <div class="common_row">
            
                <label class="col-md-4">Upload Photo</label>
                <div class="col-md-8">
                   Select images: <input type="file" id="activity-type-image" name="img" >
                    <!-- <input type="file" class="hidden" name="img" id="activity-type-image" accept="image/*" /> -->
                    <div class="crop-wrapper">
                        <img class="image-div" name ='image' src="{{url('img/avatar.png', array(), true)}}" id="profile_image" />
                    </div>
                    <input type="text" class='hidden' name="activity_id" id="activity_id" value=""/>
                    <input type="text" class='hidden' name="activity_photo_id" id="activity_photo_id" value=""/>

                </div>
            </div>
            <div class="common_row">
                <div class="col-md-3 col-md-offset-9">
                    <input class="btn btn-success pull-right" type='submit' id ='upload_type_image'></input>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>




          

<div class="modal" id="edit_main_activity_detail" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Update Activity Main Details</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">

<p style='display:none' class='alert alert-success' id='update'><p>
<div class="">
  <div id='error_message'></div>
  <div class="row">
      <!-- edit form column -->
      <div class="col-md-12 personal-info">
         <form >
                        {!! csrf_field() !!}
      <input name='activity_id' type='hidden' value="{{$details->activity_id}}">
 
          <div class="form-group row">
            <label class="col-lg-3 control-label">Activity Name :</label>
            <div class="col-lg-8">
              <input class="form-control"  name="activity_name" type="text" value="{{$details->activity_name}}">
            </div>
          </div>

                    <div class="form-group row">
            <label class="col-lg-3 control-label" style='margin-right:16px'>Activity Type:</label>
          <select id ='activity-type' class='form-control' name='activity_type' style="margin-left: 15px;height:32px;width:200px" >
              <option  value='not_include' disabled selected>Activity Type</option>
          @foreach ($activitytypesglobal as $activity)
            <option value= "{{$activity->id }}" >{{$activity->type}}</option>
          @endforeach
          </select>
          </div>


          <div class="form-group row" id ='activity_type-item-list-div-block' style='display:none'>
            <label class="col-lg-3 control-label">
            </label>
            <div class="col-lg-8" id ='activity-type-div-block'>
              @foreach ($activitytypesglobal_dict as $activity)
              <div class="activity-type selected_item" name="activity_div[]">
                <a style="cursor: pointer;padding-left: 4px;padding-right: 7px;">x</a>
                <span value="{{$activity->id}}">{{$activity->type}}</span>
                <input type="hidden" name="activity_type[]" value="{{$activity->id}}">
              </div>
             @endforeach
            </div>
          </div>
            <div class="form-group suggestion_add_activity" style="margin-right: 17%;margin-left: 17%;border-top-width: 7px;margin-top: 5px;">
            Select activity type from the dropdown. You may select multiple activity types if your activity includes more than 1 kind of activity. For Example: Trekking, Sight-seeing, River Rafting. 
            </div> 

<!-- activitytypesg -->
                  
          <div class="form-group row">
            <label class="col-lg-3 control-label" style='margin-right:16px' >Activity Tags:</label>
          <select id ='activity-tag' class='form-control' name='activity_tag' style="margin-left: 15px;height:32px;width:200px">
              <option  value='not_include' disabled selected>Activity Tags</option>
          @foreach ($activitytagsglobal as $activitytag)
            <option value= "{{$activitytag->id }}" >{{$activitytag->tag}}</option>
          @endforeach
          </select>
          </div>

          <div class="form-group row" id ='activity_tag-item-list-div-block' style='display:none'>
            <label class="col-lg-3 control-label">
            </label>
            <div class="col-lg-8" id ='activity-tag-div-block'>
               @foreach ($activitytagsglobal_dict as $activitytag)
              <div class="activity-tag selected_item">
                <a style="cursor: pointer;padding-left: 4px;padding-right: 7px;">x</a>
                <span value="{{$activitytag->id }}">{{$activitytag->tag}}</span>
                <input type="hidden" name="activity_tag[]" value="{{$activitytag->id}}">
              </div>
              @endforeach
            </div>
          </div>

          <div class="form-group suggestion_add_activity" style="margin-right: 17%;margin-left: 17%;border-top-width: 7px;margin-top: 5px;">
            Select activity tags from the dropdown. You may select multiple tags. For Example: Family, Corporate, Easy, Hard etc. 
          </div>  


            <div class="form-group row">
            <label class="col-lg-3 control-label" style='margin-right:16px'>Country:</label>

          <select name ='country' style='margin-left: 15px;height:32px;width:200px' class="form-control" id='country'>
              <option  value='not_include' disabled selected>Country</option>
          @foreach ($countries as $country)
            <option value= "{{ $country->id}}" >{{$country->name }}</option>
          @endforeach
          </select>
          </div>

            <div class="form-group row">
            <label class="col-lg-3 control-label" style='margin-right:16px'>State:</label>

            <select name ='state' id='state' class="form-control" style="margin-left: 15px;height:32px;width:200px" >
              <option  value='not_include' selected disabled>State</option>
            @foreach ($states as $state)

              <option data-id="{{ $state->id}}">{{ucwords(strtolower($state->name)) }}</option>
            @endforeach
            </select>
          </div>
            
          <div class="form-group row">
            <label class="col-lg-3 control-label" style='margin-right:16px'>City:</label>
            <select name ='city' id='city' style='margin-left: 15px;height:32px;width:200px' class="form-control">
              <option  value='not_include' selected disabled>City</option>

            </select>
          </div>
          

          <div class="form-group row">
            <label class="col-lg-3 control-label">Description :</label>
            <div class="col-lg-8">
              <textarea class="form-control" name="descriptions" 
              type="text" value="{{$details->descriptions}}">{{$details->descriptions}}</textarea>
            </div>
          </div>
            
           
           <div class="form-group row">
            <label class="col-lg-3 control-label">What to Expect:</label>
            <div class="col-lg-8">
              <input class="form-control"  name="expect" type="text" value="{{$details->instructions['what_to_expect']}}">
            </div>
            <!-- <a style='cursor: pointer; 'id ='add-include'>Add</a> -->
          </div>

          <div class="form-group row">
            <label class="col-lg-3 control-label">What you will do:</label>
            <div class="col-lg-8">
              <input class="form-control"  name="what_you_will_do" type="text" value="{{$details->instructions['what_you_will_do']}}">
            </div>
            <!-- <a style='cursor: pointer; 'id ='add-include'>Add</a> -->
          </div>
          
          @if ($details->instructions['what_to_include'])
          <div class="form-group row">
            <label class="col-lg-3 control-label">What is Included:</label>
            <div class="col-lg-8">
              <input class="form-control"  name="instruction[]" type="text" value="{{$details->instructions['what_to_include'][0]}}">
            </div>
            <a style='cursor: pointer; 'id ='add-include'>Add</a>
          </div>
           <div id = 'append-include'>
            @for ($i=1;$i<count($details->instructions['what_to_include']);$i++)
              <div id ='append-include'></div>
                <div class="form-group row">
                  <label class="col-lg-3 control-label"></label>
                  <div class="col-lg-8">
                    <input class="form-control"  name="instruction[]" type="text" value="{{$details->instructions['what_to_include'][$i]}}">
                    <a style="cursor: pointer;" id ="remove-include">remove</a>
                  </div>
                </div>
            @endfor
            </div>
          @endif
          
           @if ($details->instructions && $details->duration_days>0 && array_key_exists('days_spec', $details->instructions)) 
                        <div class="form-group row" id ='itinerary_div'>
                          <label class="col-md-3 control-label">Itinerary</label>
              <div class="col-md-8">
              <textarea id="itinerary"  name="itinerary"  placeholder="Enter text ...">{!! $details->instructions['days_spec'] !!}</textarea>
                        </div>
                        </div> 
          @endif



              
          <div class="form-group row">
            <label class="col-lg-3 control-label">Cancellation Policy:</label>
            <div class="col-lg-8">
              @if ($details->instructions && array_key_exists('cancellation_policy', $details->instructions) && $details->instructions['cancellation_policy']!="" )
              <input class="form-control" placeholder="No of days untill which the customer can cancel the booking without a fee"  name="cancellationpolicy" type="text" value="{{$details->instructions['cancellation_policy']}}">
              @else
              <input class="form-control" placeholder="No of days untill which the customer can cancel the booking without a fee"  name="cancellationpolicy" type="text" value="">
              @endif
            </div>
            <!-- <a style='cursor: pointer; 'id ='add-include'>Add</a> -->
          </div>
          <div id ='append-cancellation-policy'></div>
           <div class="form-group suggestion_add_activity" style="margin-right: 17%;margin-left: 26%;border-top-width: 7px;margin-top: 5px;">
          For Example: If you enter "4" then policy would be : <br>
            Cancellation Policy <br>
            Upto 4 days before the start of the activity: Full Refund <br>
            Less than 4 days before the start of the activity: No Refund
          </div>  
          

          <div class="form-group row">
            <label class="col-lg-3 control-label">Comment</label>
            <div class="col-lg-8">
              @if (array_key_exists('comment',$details->instructions))
              <input class="form-control" placeholder="Special comment for your activity"  name="comment" type="text" value="{{$details->instructions['comment']}}">
              @else
                <input class="form-control" placeholder="Special comment for your activity"  name="comment" type="text" value="">
              @endif
            </div>
            <!-- <a style='cursor: pointer; 'id ='add-cancellation-policy'>Add</a> -->
          </div>

           
           <div class="form-group row">
            <label class="col-lg-3 control-label">Zip code :</label>
            <div class="col-lg-8">
              <input class="form-control" name="zip_code" type="text" value="{{$details->zipcode}}">
            </div>
          </div>

<!--           <div class="form-group row">
            <label class="col-lg-3 control-label">Instructor Name :</label>
            <div class="col-lg-8">
              <input class="form-control" name="instructor_name" type="text" value="">
            </div>
          </div> -->

          <div class="form-group row">
            <label class="col-lg-3 control-label">Address Line 1 :</label>
            <div class="col-lg-8">
              <input class="form-control" name="address_line_1" type="text" value="{{$details->address_line_1}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-lg-3 control-label">Address Line 2 :</label>
            <div class="col-lg-8">
              <input class="form-control" name="address_line_2" type="text" value="{{$details->address_line_2}}">
            </div>
          </div>


          <div class="form-group row">
            <label class="col-lg-3 control-label">Landmark :</label>
            <div class="col-lg-8">
              <input class="form-control" name="landmark" type="text" value="{{$details->landmark}}">
            </div>
          </div>


          <div class="form-group row">
            <!-- <label class="col-md-2 control-label"></label> -->
            <div class="col-md-5">
              <button type="submit" id='update_main_activity' style="width: 100%;" class="btn btn-success">Update Activity
              </button>
            </div>
          </div>
     </form>
      </div>

  </div>
</div>
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>


    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-11 col-lg-11 col-md-11 row">
                        <div style="padding-left:50px;">
                          
                            <h4>Activity Details <a href="#" class="open-activity-type-edit"  data-toggle="modal" data-target="#edit_main_activity_detail">Edit</a></h4>
                            <!--div class="form-group row">
                                <label for="f-name" class="col-md-4 col-form-label lead">First Name</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" value="Priyank Tewari" id="f-name">
                                </div>
                            </div-->    
                                        <!-- <h3><strong>Activity Name:</strong>{{$details->activity_name}}</h3> -->
            <!-- h3><strong>City:</strong>{{$details->city_name}}</h3>
            <h3><strong>State:</strong>{{$details->state_name}}</h3>
            <h3><strong>Country:</strong>{{$details->country_name}}</h3>
            <h3><strong>Address Line 1:</strong>{{$details->address_line_1}}</h3>
            <h3><strong>Address Line 2:</strong>{{$details->address_line_2}}</h3> -->

                            <p><span class="col-md-4">Activity Name</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->activity_name}}</p>
                            <p><span class="col-md-4">Activity Duration</span> :&nbsp;&nbsp;&nbsp;&nbsp;
                              @if ($details->duration_days) 
                              {{$details->duration_days}} Days
                              @endif
                              
                              @if ($details->duration_hours)
                                {{$details->duration_hours}} Hours
                              @endif 
                              
                              @if ($details->duration_minutes)
                              {{$details->duration_minutes}} Minutes
                              @endif

                            </p>
                            <p><span class="col-md-4">Description</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->descriptions}}</p>
                            <p><span class="col-md-4">Activity Type</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{join(',', $activitytypesglobal_list)}}</p>
                            <p><span class="col-md-4">Activity Tags</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{join(',', $activitytagsglobal_list)}}</p>
                           
                            <!-- <p><span class="col-md-4">Number Of Seats</span> :&nbsp;&nbsp;&nbsp;&nbsp;10</p> -->
                            <!-- <p><span class="col-md-4">Are Kids Allowed</span> :&nbsp;&nbsp;&nbsp;&nbsp;Yes</p> -->
                            <!-- <p><span class="col-md-4">Number Of Kids Allowed</span> :&nbsp;&nbsp;&nbsp;&nbsp;4</p> -->
                            <!-- <p><span class="col-md-4">Weather Type</span> :&nbsp;&nbsp;&nbsp;&nbsp;Sunny/ Cloudy</p> -->
                            <!-- <p><span class="col-md-4">Offered On</span> :&nbsp;&nbsp;&nbsp;&nbsp;Everyday</p> -->

                            <p><span class="col-md-4">Activity Address Line 1</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->address_line_1}}</p>
                            <p><span class="col-md-4">City</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->city_name}}</p>
                            @if ($details->instructions && array_key_exists('what_to_expect',$details->instructions) && $details->instructions['what_to_expect'])
                            <p><span class="col-md-4">What To Expect</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->instructions['what_to_expect']}}</p>
                            @else
                            <p><span class="col-md-4">What To Expect</span> :&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            @endif

                            @if ($details->instructions && array_key_exists('what_to_include',$details->instructions) && $details->instructions['what_to_include'])
                            <p><span class="col-md-4">What To Include</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{join(',',$details->instructions['what_to_include'])}}</p>
                            @else
                            <p><span class="col-md-4">What To Include</span> :&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            @endif

                            @if ($details->instructions && array_key_exists('what_you_will_do',$details->instructions) && $details->instructions['what_you_will_do'])
                            <p><span class="col-md-4">What You Will Do</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->instructions['what_you_will_do']}}</p>
                            @else
                            <p><span class="col-md-4">What You Will Do</span> :&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            @endif

                             @if ($details->instructions && array_key_exists('days_spec',$details->instructions) && $details->instructions['days_spec'])
                            <p><span class="col-md-4">Itinerary</span> :&nbsp;&nbsp;&nbsp;&nbsp;{!!$details->instructions['days_spec']!!}</p>
                            @else
                            <p><span class="col-md-4">Itinerary</span> :&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            @endif



                            @if ($details->instructions && array_key_exists('comment',$details->instructions) && $details->instructions['comment'])
                            <p><span class="col-md-4">Comment</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->instructions['comment']}}</p>
                            @else
                            <p><span class="col-md-4">Comment</span> :&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            @endif



                            @if ($details->instructions && array_key_exists('cancellation_policy', $details->instructions) && $details->instructions['cancellation_policy']!="" )
                               <p><span class="col-md-4">Cancellation Policy</span> :&nbsp;&nbsp;&nbsp;&nbsp;Upto {{$details->instructions['cancellation_policy']}} days before the start of the activity: Full Refund. 
                                Less than {{$details->instructions['cancellation_policy']}} days before the start of the activity: No Refund
                            @endif
  

                            <p><span class="col-md-4">Activity Address Line 2</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->address_line_2}}</p>
                            <p><span class="col-md-4">City</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->city_name}}</p>
                            <p><span class="col-md-4">State</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->state_name}}</p>
                            <p><span class="col-md-4">Country</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->country_name}}</p>
                            <p><span class="col-md-4">Pincode</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->zipcode}}</p>
                            <p><span class="col-md-4">Landmark</span> :&nbsp;&nbsp;&nbsp;&nbsp;{{$details->landmark}}</p>
                            
                            <hr>
                            
                                <!-- <u><h4>Instructor Details</h4></u>
                                <p><span class="col-md-4">Phone Number</span> :&nbsp;&nbsp;&nbsp;&nbsp;+91-9871123456</p>
                                <p><span class="col-md-4">EMail Address</span> :&nbsp;&nbsp;&nbsp;&nbsp;abc@lokll.com</p>
                                <p><span class="col-md-4">Address Line 1</span> :&nbsp;&nbsp;&nbsp;&nbsp;Street Number 4</p>
                                <p><span class="col-md-4">Address Line 2</span> :&nbsp;&nbsp;&nbsp;&nbsp;House Number 10</p>
                                <p><span class="col-md-4">City</span> :&nbsp;&nbsp;&nbsp;&nbsp;Gurgaon</p>
                                <p><span class="col-md-4">State</span> :&nbsp;&nbsp;&nbsp;&nbsp;Haryana</p>
                                <p><span class="col-md-4">Pincode</span> :&nbsp;&nbsp;&nbsp;&nbsp;122001</p>
                                <p><span class="col-md-4">Country</span> :&nbsp;&nbsp;&nbsp;&nbsp;India</p> -->
                            
                            <hr>
<!--                             <u><h4>Bank Account Details</h4></u>
                            <p><span class="col-md-4">Account Number</span> :&nbsp;&nbsp;&nbsp;&nbsp;A12345TRFT436384</p>
                            <p><span class="col-md-4">IFSC</span> :&nbsp;&nbsp;&nbsp;&nbsp;PUNB123</p>
                            <p><span class="col-md-4">Bank Name</span> :&nbsp;&nbsp;&nbsp;&nbsp;Punjab National Bank</p>
                            <p><span class="col-md-4">Tax Id NUmber</span> :&nbsp;&nbsp;&nbsp;&nbsp;12356644567</p>
                            <p><span class="col-md-4">Government Id Number</span> :&nbsp;&nbsp;&nbsp;&nbsp;67432236985897</p>
                            <hr>
 -->                         
                            <!-- <u><h4>Activity Images</h4></u> -->
                            <!-- <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#activity-images">View Activity Images</button> -->

 <h4>Activity Photos <a href="#" class="open-activity-photo-edit"  data-aid="{{$details->activity_id}}" data-photo_id="" style="display:inline-block;font-size: small;" >Add</a></h4>                                        
  <input type="hidden" name='aid' id="aid" 
                value="{{$details->activity_id}}"/>

                <div id="sliderFrame">
                <div id="slider">
                @foreach($activity_image_urls as $image)
                          <a class="lazyImage" href="{{url('/img/activityphoto/', array(), true).'/'.$image['url']}}"></a>
                @endforeach                
                </div>
                <div style="display: none;">
                    <div id="htmlcaption3">
                        <em>HTML</em> caption. Back to <a href="https://www.lokll.com/">Lokll</a>.
                    </div>
                    <div id="htmlcaption5">
                        Explore like Locals
                    </div>
                </div>
        <!--thumbnails-->
        <div id="thumbs">
            
            @foreach($activity_image_urls as $image)
           <div style='display:inline-block'>
          <!--   @if ($image['url']!='null.jpg')
                 <a class='button_delete' data-aid="{{$details->activity_id}}" data-photo_id="{{$image['photo_id']}}" style='display:inline-block' href='#'>Delete</a>
            @endif -->


            <a class='button_delete' data-aid="{{$details->activity_id}}" data-photo_id="{{$image['photo_id']}}" style='display:inline-block' href='#'>Delete</a>
            
            <a class="open-activity-photo-edit"  data-aid="{{$details->activity_id}}" data-photo_id="{{$image['photo_id']}}" style="display:inline-block;font-size: small;" href="#">Edit</a>
            <div class="thumb">
            <img src="{{url('/img/activityphoto/', array(), true).'/'.$image['url']}}" /></div>
            </div>
             @endforeach
        </div>
    </div>

<h3>Time slot <a class="btn btn-success activity-add-slot"  data-aid="{{$details->activity_id}}" data-photo_id="{{$image['photo_id']}}" style="display:inline-block;font-size: small;" >Add</a>
<a class="btn btn-success activity-search-edit-slot"  data-aid="{{$details->activity_id}}" data-photo_id="{{$image['photo_id']}}" style="display:inline-block;font-size: small;" >Edit</a>
<a class="btn btn-success activity-replicate-slot"  data-aid="{{$details->activity_id}}"  style="display:inline-block;font-size: small;">Bulk slot edit</a>
<!--
<a class="btn btn-success activity-replicate-across-date"  data-aid="{{$details->activity_id}}"  style="display:inline-block;font-size: small;">Replicate Across Date</a>
<a class="btn btn-success activity-replicate-accross-slot"  data-aid="{{$details->activity_id}}"  style="display:inline-block;font-size: small;">Replicate Across Slot</a>
-->
<a class="btn btn-success activity-delete-slot"  data-aid="{{$details->activity_id}}"  style="display:inline-block;font-size: small;">Delete Slot</a>


</h3>

<div class="modal" id="replcate_slot_modal" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Replicate Details to all Slot</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
      @include('admin.replicate_slot_form')
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>

<div class="modal" id="add_slot_modal" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Change</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
      @if ($details->duration_days>0)
        @include('admin.timeslot_add_form_multipledays')
      @else
        @include('admin.timeslot_add_form')
      @endif
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>

<div class="modal" id="search_edit_slot" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Change</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
      @include('admin.search_slot_edit_form')
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>

<div class="modal" id="activity_replicate_across_slot" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Change</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
      @include('admin.activity_replicate_across_slot_form')
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>

<div class="modal" id="activity_delete_slot" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Delete SLot</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
         <label class="col-lg-3 control-label">Distinct Slot :</label>
            <div class="col-lg-8">
              <ul>
                @foreach($distinct_slot as $slot)
                <li>
                  {{$slot->start_time}}-{{$slot->end_time}} 
                  <a type='checkbox' class='delete-slot' value='{{$slot->diff}}'  
                        data-end_time = "{{$slot->e_time}}" 
                        data-start_time = "{{$slot->s_time}}" name="slot[]">Delete
                  </a>

                </li>
                @endforeach
              </ul>
            </div>
        <!-- </div> <--></-->
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>




<div class="modal" id="activity_replicate_across_date" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" data-backdrop="false">&times;</button>
      <h4 class="modal-title">Date</h4>
    </div>
    <div class="modal-body type-photo-modal">
    <div class="form_layout">
      @include('admin.activity_replicate_across_date_form',array('distinct_slot'=>$distinct_slot))
    </div>
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>




                            <u><h4 style="">Activity Calendar</h4></u>
                            <div id='calendar_edit' style="margin-top: 2%;">

                            <!------>
                        
                            
                            <!------>
                            
                            
                            
                            <!----->
                        </div>
                        <!--div class="col-sm-6 col-lg-6 col-md-6">
                            
                            <div class="thumbnail">
                                <div id="revenue-charts">
                                    <table class="table table-striped">
                                        <thead>
                                                <tr>
                                                  <th>#</th>
                                                  <th>Activity</th>
                                                  <th>Available</th>
                                                  <th>Slots</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tr>
                                                  <th scope="row">1</th>
                                                  <td>Mark</td>
                                                  <td>Otto</td>
                                                  <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">2</th>
                                                  <td>Jacob</td>
                                                  <td>Thornton</td>
                                                  <td>@fat</td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">3</th>
                                                  <td>Larry</td>
                                                  <td>the Bird</td>
                                                  <td>@twitter</td>
                                                </tr>
                                              </tbody>
                                    </table>
                                </div>
                            </div>
                        </div-->
                        <!------------->
                                
                        <!--div class="col-sm-4 col-xs-4 col-md-4" >

                            <div class="tile-stats tile-green" style="margin-left:15px;">
                                <div class="icon"><i class="entypo-chart-bar"></i></div>
                                <div class="num" data-start="0" data-end="135" data-postfix="" data-duration="1500" data-delay="600">100M</div>

                                <h3>Daily Visitors</h3>
                            </div>

                        </div>

                            <div class="col-sm-4 col-xs-4">

                                <div class="tile-stats tile-aqua">
                                    <div class="icon"><i class="entypo-mail"></i></div>
                                    <div class="num" data-start="0" data-end="23" data-postfix="" data-duration="1500" data-delay="1200">5</div>

                                    <h3>New Messages</h3>
                                </div>

                            </div>

                            <div class="col-sm-4 col-xs-4">

                                <div class="tile-stats tile-blue" style="margin-right:15px;">
                                    <div class="icon"><i class="entypo-rss"></i></div>
                                    <div class="num" data-start="0" data-end="52" data-postfix="" data-duration="1500" data-delay="1800">10M</div>

                                    <h3>Subscribers</h3>
                                </div>

                            </div>
                            <!--/div-->
                            <br><br><br><br>
                            <!------------>
                        <!--div class="row">
                          <h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manage</h2>
                          
                          <div class="col-xs-6 col-sm-4" style="text-align:center;" >

                            <img src="img/bookings.png" alt="Booking" width="184" height="184" />
                            <h2 class="text-center">Bookings</h2>
                              </div>
                          <div class="col-xs-6 col-sm-4" style="text-align:center;" >
                            <img src="img/activity.png" width="184" height="184" alt="Activity"/> 
                            <h2 class="text-center">Activities</h2>
                            <h2 class="text-center">Activitie</h2>
                            </div>
                          <div class="col-xs-6 col-sm-4" style="text-align:center;" >
                            Activity<img src="img/finance.png" width="184" height="184" alt="Finance"/>
                            <h2 class="text-center">Finance</h2> </div>
                        </div>
                        <br><br-->

                            <!------------>
                        <!--/div-->
                    </div>



                    <!--div class="col-sm-3 col-lg-3 col-md-3">
                        <a href="#"><p class="pull-right sub-title"><sub>T&C*</sub></p></a>
                        <h4>Recommended Items</h4>
                        <div class="thumbnail">
                            <img src="img/destination-2.jpg" alt="">
                            <div class="caption">
                                <h6 class="pull-right">₹740</h6>
                                <h6><a href="#">Kamal Homestays @Nainital</a>
                                </h6>
                                <p class="small">Our house is a good place to have weed and enjoy life. GetGoodLife!</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right small">31 reviews</p>
                                <p>
                                    <span class="small glyphicon glyphicon-star"></span>
                                    <span class="small glyphicon glyphicon-star"></span>
                                    <span class="small glyphicon glyphicon-star"></span>
                                    <span class="small glyphicon glyphicon-star"></span>
                                    <span class="small glyphicon glyphicon-star-empty"></span>
                                </p>
                            </div>
                        </div>
                    </div-->




                </div>

            </div>

        </div>

    </div>
</div>
<script>

  $(document).on("click", ".open-activity-photo-edit", function () {
      var activity_id = $(this).data('aid');
      var photo_id = $(this).data('photo_id');
      console.log('activity')
      console.log(activity_id);
        $("#activity_photo_id").val(photo_id);
        $("#activity_id").val(activity_id);
        
        $('#add_activity_photo_modal').modal('show');
        
      });

  $(document).on("click", ".activity-replicate-slot", function () {
      var activity_id = $(this).data('aid');
      var photo_id = $(this).data('photo_id');
      console.log('activity')
      console.log(activity_id);
        $("#activity_photo_id").val(photo_id);
        $("#activity_id").val(activity_id);
        
        $('#replcate_slot_modal').modal('show');
        
      });

    $(document).on("click", ".activity-replicate-accross-slot", function () {
      console.log('activity')
      console.log(activity_id);
        $("#activity_id").val(activity_id);        
        
        $('#activity_replicate_across_slot').modal('show');
        
      });


    $(document).on("click", ".activity-delete-slot", function () {
        $("#activity_id").val(activity_id);        
        $('#activity_delete_slot').modal('show');
        
      });


     $(document).on("click", ".activity-replicate-across-date", function () {
      console.log('activity')
      console.log(activity_id);
        $("#activity_id").val(activity_id);        
        
        $('#activity_replicate_across_date').modal('show');
        
      });

  

    $(document).on("click", ".activity-add-slot", function () {
      var activity_id = $(this).data('aid');
      var photo_id = $(this).data('photo_id');
      console.log('activity')
      console.log(activity_id);
        $("#activity_photo_id").val(photo_id);
        $("#activity_id").val(activity_id);
        
        $('#add_slot_modal').modal('show');
        
      });

        $(document).on("click", ".activity-search-edit-slot", function () {
      var activity_id = $(this).data('aid');
      var photo_id = $(this).data('photo_id');
      console.log('activity')
      console.log(activity_id);
        $("#activity_id").val(activity_id);
        $('#search_edit_slot').modal('show');
        
      });


$('#activity-imagesmyModal,#add_activity_photo_modal,#edit_main_activity_detail,#replcate_slot_modal,#add_slot_modal,#search_edit_slot,#activity_replicate_across_slot,#activity_replicate_across_date').
      on('hidden.bs.modal', function () {
    $('.modal-backdrop').remove();
    // location.reload()
})
  
  $('#state option:contains("{{ucwords(strtolower($details->state_name))}}")').prop('selected',true)

var state_id = $('#state option:contains("{{ucwords(strtolower($details->state_name))}}")').data('id')

console.log('state_id');
console.log(state_id);

$.get('/state/get_city',{'state_id':state_id},function(d){ 
          // d = JSON.parse(d);
          for(i=0;i<d.length;i++){
            var s= document.getElementById('#city');
            $("#city").append("<option>"+d[i]['name']+"</option>");
          }
}).done(function() {


$('#city option:contains({{ucwords(strtolower($details->city_name))}})').prop('selected',true)
})
console.log('{{ucwords(strtolower($details->city_name))}}');
console.log('{{ucwords(strtolower($details->state_name))}}');


//Creata Calendar And Related Event in The Function
  $(document).ready(function(){

     $('#edit_main_activity_detail').on('hidden.bs.modal', function () {
  $('.modal-backdrop').remove();
})


            var base = 'https://'+window.location.host;
            var url = base+'/admin/getcalendar?activity_id='+$('#aid').val();
            $('#calendar_edit').fullCalendar({
            height: 550,
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'list,month,basicWeek,basicDay'
            },
            disableDragging: true,
            // editable: true,
            eventLimit: true, 
            events: url,
             eventMouseover: function (data, event, view) {
                  start_time = (data.start).format("dddd, MMMM Do YYYY, h:mm:ss a");
                  end_time = (data.end).format("dddd, MMMM Do YYYY, h:mm:ss a");

                  tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#dceff5;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">'
                  tooltip = tooltip + 'Activity: ' + ': ' + data.title + '</br>' +'Start: ' + ': ' + start_time
                  tooltip = tooltip +'</br>' +'End: ' + ': ' + end_time + '</div>';


            $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });

          },

          eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);

            $('.tooltiptopicevent').remove();
          },

          // dayClick: function(date, allDay, event, view) {
          //     var url = 'https://'+window.location.host+'/vendor/booking/create_timeslot/';
          //     $.ajax({
          //        url: url,
          //        type: 'GET',
          //     success: function(data){
          //         console.log('krishna');
          //         $("#modal-body").html(data);
          //          $('#myModal').modal('show');
          //         $("#create_timeslot").html(data);
          //             //To enable date 
          //             // $("#start_date").datepicker({
          //             //   dateFormat: 'yy-mm-dd',
          //             //  });
          //             //  $("#end_date").datepicker({
          //             //  dateFormat: 'yy-mm-dd',
          //             //  });
          //         // $('#myModal').modal('show');
          //         // $("#eventLink").attr('href', event.url);
          //         // $("#create_timeslot").dialog({ 
          //         //   modal: true, 
          //         //   width: 700,
          //         //   // height: 500,
          //         //   title: 'Time Slot Details Add',
          //         //   close: function( event, ui ) {
          //         //     location.reload();
          //         //   }

          //         //   });
          //     }
          //   });

          // }
          // ,
          eventClick:function(event){
              var url = 'https://'+window.location.host+'/motherboard/booking/activity/';
              $.ajax({
                 url: url,
                 type: 'GET',
                 data:{ 
                        'timetable':event.timetable_id,
                        'json':1,
                        'activity_id':event.activity_id,
                        'vendor_id':event.vendor_id
                      },
              success: function(data){
                  //Activity Name From event
                  $("#modal-body").html(data);
                    $('#myModal').modal('show');
              }
            });

          }
});





// console.log(state_id);
  // $('#state').val(state_id)
  $('body').on('click', '#add-include', function(){
    console.log('clicked')
    string = '<div class="form-group row">'+
         '<label class="col-lg-3 control-label"></label>'+
         '<div class="col-lg-8">' +
          '<input class="form-control"  name="instruction[]" type="text" value="">'+
          '<a style="cursor: pointer;" id ="remove-include">remove</a>'+
          '</div>'+ 
         ' </div>'
    if ($('#append-include').children().length==2){
      $('#add-include').hide()
    } 
    $('#append-include').append(string)
  });

  $('body').on('click', '#remove-include', function(){
      $(this).parent().parent().remove();
      if ($('#append-include').children().length<=2){
        $('#add-include').show()
      } 
  });

 $('body').on('click', '#add-cancellation-policy', function(){
    string = '<div class="form-group row">'+
         '<label class="col-lg-3   control-label"></label>'+
         '<div class="col-lg-8">' +
          '<input class="form-control"  name="cancellationpolicy[]" type="text" value="">'+
          '<a style="cursor: pointer;" id ="remove-cancellation-policy">remove</a>'+
          '</div>'+ 
         ' </div>'
    if ($('#append-cancellation-policy').children().length==2){
      $('#add-cancellation-policy').hide()
    } 
    $('#append-cancellation-policy').append(string)
  });

  $('body').on('click', '#remove-cancellation-policy', function(){
      $(this).parent().parent().remove();
      if ($('#append-cancellation-policy').children().length<=2){
        $('#add-cancellation-policy').show()
      } 
  });


  $('#activity_type').val("{{$details->type}}")
  $('#city').val("{{$details->city}}")
  $('#country').val("{{$details->country}}")

  $("#state").change(function () {
          var selectedText = $(this).find("option:selected").val();
          var selectedText = $(this).find("option:selected").data('id')

          $('#city').children().remove();
          $('#city').append("<option  value='not_include' selected disabled>City</option>")
          $.get('/state/get_city',{'state_id':selectedText},function(d){ 
          // d = JSON.parse(d);
          for(i=0;i<d.length;i++){
            var s= document.getElementById('#city');
            $("#city").append("<option>"+d[i]['name']+"</option>");
         } 
      });
  });


   $().ready(function() {
            jQuery.validator.addMethod("activity_type_check", function(value, element) {
                length = $('.activity-type.selected_item').length
                if (length>0){
                  return true;
                }
                else{
                  return false;  
                 }
            }, "* This Field is Required");

            jQuery.validator.addMethod("activity_tag_check", function(value, element) {
                length = $('.activity-tag.selected_item').length
                if (length>0){
                  return true;
                }
                else{
                  return false;  
                 }
            }, "* This Field is Required");

            $.validator.addMethod(
                  "regex",
                    function(value, element, regexp) {
                        var re = new RegExp(regexp);
                        return re.test(value);
                    },
                    "Please check your input."
              );
            $("#edit_main_details_form").validate({
                ignore: "",
                highlight: function(element, errorClass) {
                  $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight:function(element, errorClass) {
                  console.log('krishna')
                  $(element).closest('.form-group').removeClass('has-error');
                },
                rules: {
                    activity_name: "required",
                    // activity_name:{regex:"^[A-Za-z0-9 ]+$"},
                    activity_tag: "required",
                    country:'required',
                    state:'required',
                    city:'required',
                    // address_line_1:'required',
                    // zip_code:{number:true,required:true},
                    zip_code:{number:true},
                    hours:'required',
                    minutes:'required',
                    descriptions:'required',
                    activity_type:{activity_type_check:true},
                    activity_tag:{activity_tag_check:true}
                },
                errorPlacement: function(error,element) {
                    return true;
                    },
                submitHandler: function(form) {
                     var url = 'https://'+window.location.host+
                              '/motherboard/activity/update_activity_main';  
                          serialize_form = $('form').serialize();
                          $('#update').hide()
                          $.ajax
                             ({
                                url: url,
                                type: 'POST', 
                                data : serialize_form,
                                success: function(data)
                                {
                                  // $('#add_slot').hide();
                                  // $('#add_activity_message').text(data);
                                  // $('.text-center').show();

                                  console.log(data);
                                  if (data['success']){
                                    $('#update').html("");
                                    $('#error_message').html("")
                                    $('#update').html('Activity Details Are Updated')
                                    $('#update').show()
                                    // location.href
                                  }

                                  if (data['error']){
                                    $('#error_message').html("")
                                    $('#update').html("")
                                    $('#update').html(data['error'])
                                  }
                                   location.reload()
                                },
                                error: function(data) {
                                  var errors = data.responseJSON;
                                  li_element = "";
                                  for (var key in errors){
                                      var attrName = key;
                                      var attrValue = errors[key];
                                      li_element = li_element + '<li>'+ attrValue[0]+'</li>';
                                  }
                                  error_list = '<ul>'+li_element+'</ul>' ;
                                  $('#error_message').html(error_list);
                                },
                              }); 
                    }
            });
  });
   });

$(".button_delete").click(function(e) {
  photo_id = $(this).data('photo_id')
  aid = $(this).data('aid')
  var confirmation = confirm("are you sure you want to delete the  photo?");
  if (confirmation) {
     var url = 'https://'+window.location.host+'/motherboard/vendor/delete_photo/{{$details->vendor_id}}';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
          'aid':aid,
          'photo_id':photo_id
        } ,
        success: function (data) {
          alert('removed');
          window.location.reload()
        },
        error: function (er) {
            console.log('Error uploading image! Error: ', er);
            alert('Error uploading image!');
        }
    });
  }
})

$(".delete-slot").click(function(e) {
  // debugger;
  start_time = $(this).data('start_time')
  end_time = $(this).data('end_time')

  var confirmation = confirm("are you sure you want to delete the  slot ?");
  if (confirmation) {
     var url = 'https://'+window.location.host+'/motherboard/vendor/delete/slot';
    $.ajax({
        type: 'POST',
        url: url,
        data:{
          'activity_id':{{$details->activity_id}},
          'end_time':end_time,
          'start_time':start_time,
          'vendor_id':{{$details->vendor_id}}
        } ,
        success: function (data) {
          alert('removed');
          window.location.reload()
        },
        error: function (er) {
            console.log('Error uploading image! Error: ', er);
            alert('Error uploading image!');
        }
    });
  }
})





  $('body').on('change', '#activity-type', function(){
    value = $(this).val();
    enabled_count = $('#activity-type').find('option:not(:disabled)').length;
    disabled_count = $('#activity-type').find('option:disabled').length;
    selected_item_value = $(this).val();
    selected_item_name = $('#activity-type :selected').text();
    $('#activity-type :selected').prop('disabled',true);
    if (parseInt(disabled_count)>=1){
      $('#activity_type-item-list-div-block').show();
      selected_item_div = '<div class="activity-type selected_item" name="activity_div[]">'+
      '<a style="cursor: pointer;padding-left: 4px;padding-right: 7px;">x</a>'+
        '<span  value='+selected_item_value+'>'+selected_item_name+'</span>'+
        '<input type="hidden" name=activity_type[] value = "'+selected_item_value+'"></input>'+
        '</div>';
      $('#activity-type-div-block').append(selected_item_div);
    }
  });


    $('body').on('click', '.activity-type.selected_item a', function(){
      // alert('krishna');
      value = $(this).next().attr('value');
      // alert(value);
      $(this).parent().remove();
      $('#activity-type option[value='+value+']').prop('disabled',false)
      disabled_count = $('#activity-type').find('option:disabled').length;
      if (parseInt(disabled_count)==1){
           $('#activity_type-item-list-div-block').hide();
      }
    });

  $('body').on('change', '#activity-tag', function(){
    value = $(this).val();
    enabled_count = $('#activity-tag').find('option:not(:disabled)').length;
    disabled_count = $('#activity-tag').find('option:disabled').length;
    selected_item_value = $(this).val();
    selected_item_name = $('#activity-tag :selected').text();
    $('#activity-tag :selected').prop('disabled',true);
    if (parseInt(disabled_count)>=1){
      $('#activity_tag-item-list-div-block').show();
      selected_item_div = '<div class="activity-tag selected_item">'+
      '<a style="cursor: pointer;padding-left: 4px;padding-right: 7px;">x</a>'+
        '<span  value='+selected_item_value+'>'+selected_item_name+'</span>'+
        '<input type="hidden" name=activity_tag[] value = "'+selected_item_value+'"></input>'+
        '</div>';
      $('#activity-tag-div-block').append(selected_item_div);
    }
  });


  $('body').on('click', '.activity-tag.selected_item a', function(){
    value = $(this).next().attr('value');
    // alert(value);
    $(this).parent().remove();
    $('#activity-tag option[value='+value+']').prop('disabled',false)
    disabled_count = $('#activity-tag').find('option:disabled').length;
    if (parseInt(disabled_count)==1){
         $('#activity_tag-item-list-div-block').hide();
    }
  });

 $('#activity_type-item-list-div-block').show();

 $('#activity_tag-item-list-div-block').show();

  @foreach ($activitytypesglobal_dict as $activity)
    $("{{'#activity-type option[value='.$activity->id.']'}}").prop('disabled',true)
  @endforeach

    @foreach ($activitytagsglobal_dict as $activity)
    $("{{'#activity-tag option[value='.$activity->id.']'}}").prop('disabled',true)
  @endforeach


  $('body').on('change', '.change-text-variable', function(){
  console.log('krishn');
  blackout_seats = $('#blackout_seats').val()
  number_of_seats_allowed = $('#number_of_seats_allowed').val()
  number_of_kids_allowed = $('#number_of_kids_allowed').val()
  if (number_of_kids_allowed && number_of_seats_allowed && blackout_seats){
      number_of_kids_allowed = parseInt(number_of_kids_allowed)
      number_of_seats_allowed = parseInt(number_of_seats_allowed)
      blackout_seats = parseInt(blackout_seats)
      number_of_adults_allowed = number_of_seats_allowed - number_of_kids_allowed - blackout_seats
      console.log(number_of_adults_allowed);
      $('#number_of_adults_allowed').val(number_of_adults_allowed)
  }
});


</script>
@stop