<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
include_once SBH_DIR . '/admin/functions/logo-header.php';
?>
<style> 
#border1 {
  border: 2px #F0F0F0 solid;
  border-radius: 15px;
  background-color:#F8F8F8;
  padding: 15px;
  margin-left: 50px;
  margin-bottom: 15px;
  box-shadow: 0px 0px;
  max-width: 300px;
}
.green_dot {
  height: 16px;
  width: 16px;
  background-color: green;
  border-radius: 50%;
  display: inline-block;
  margin-left:10px;
  margin-right:20px;
}
.red_dot {
  height: 16px;
  width: 16px;
  background-color: red;
  border-radius: 50%;
  display: inline-block;
  margin-left:10px;
  margin-right:20px;
}
</style>
<?php

// get_bloginfo( 'version' );
global $wp_version;
$php_version = phpversion();
ob_start();
phpinfo(INFO_MODULES);
$contents = ob_get_clean();
$moduleAvailable = strpos($contents, 'mod_security') !== false;
$plugin_conflicted = 0;
$theme_conflicted = 0;
$theme = wp_get_theme();
?>

<h1 style="padding-left:50px;padding-bottom:50px;"> Stylish Business Hour: Diagnostic </h1>
<?php
// PHP VERSION HIGHER THAN 5.0
	if ( version_compare( $php_version, '5.5', '>=' ) ) {
		echo '<div id="border1"><span class="green_dot"></span><span style="font-size:1em;font-weight:bold;">PHP Version:</span> '. $php_version. '<br></div>';
	}else{
		echo '<div id="border1"><span class="red_dot"></span><span style="font-size:1em;font-weight:bold;">PHP Version:</span> ' .$php_version. '<br></div>';
	}
	

// WP VERSION HIGHER THAN 5.0
	if ( version_compare( $wp_version, '5.0', '>=' ) ) {
		// WordPress version is greater than 4.3
		echo '<div id="border1"><span class="green_dot"></span> <span style="font-size:1em;font-weight:bold;">WordPress Version:</span> '. $wp_version. '</div>';
	}else{
		echo '<div id="border1"><span class="red_dot"></span> <span style="font-size:1em;font-weight:bold;">WordPress Version:</span> '. $wp_version. '</div>';
		
	}


// MOD SECURITY IS OFF
	if ($moduleAvailable == NULL){
		echo '<div id="border1"><span class="green_dot"></span> <span style="font-size:1em;font-weight:bold;">MOD Security:</span> Off </div> '; 
	}else{
		echo '<div id="border1"><span class="red_dot"></span> <span style="font-size:1em;font-weight:bold;">MOD Security:</span>'. $moduleAvailable. '<br></div>';
	}
	
//Check SiteOrigin Plugin active or not
	//function my_custom_admin_notice(){
	//	if ( is_plugin_active( 'stylish-business-hours/stylish-business-hours.php' ) ) { //plugin is activated
	//	$plugin_conflicted = $plugin_conflicted + 1;
		/*?>
		  <div class="error notice">
			<p>We noticed you have Siteorigin Pagebuilder active, this can cause conflicts with the colors of the Price List. Make sure you are using the CUSTOM HTML WIDGET and not SITE ORIGIN EDITOR</p>
		  </div>
	   <?php*/
//	   } 

//	}
	// add_action( 'admin_notices', 'my_custom_admin_notice' );
//End SiteOrigin

//Check if Stylish Price List
if ( is_plugin_active( 'siteorigin-panels/siteorigin-panels.php' ) ) { //plugin is activated
		$plugin_conflicted = $plugin_conflicted + 1;
		$plugins_detected = "Page Builder by SiteOrigin";
}

// Check if any plugins are conflicted
	
	if ($plugin_conflicted== NULL){
		echo '<div id="border1"><span class="green_dot"></span> <span style="font-size:1em;font-weight:bold;">Conflicted Plugins:</span> None </div>'; 
	}else{
		echo '<div id="border1"><span class="red_dot"></span> <span style="font-size:1em;font-weight:bold;">Conflicted Plugins:</span> '. $plugin_conflicted. '<br></div>';
        echo "The plugins you have that might conflict are: $plugins_detected";
	}

//if($theme == 'Divi'){
 //   $theme_conflicted = $theme_conflicted + 1;
//}

// Check if any themes are conflicted
	
	//if ($theme_conflicted== NULL){
	//	echo '<div id="border1"><span class="green_dot"></span> <span style="font-size:1em;font-weight:bold;">Conflicted Theme:</span> None </div>'; 
///	}else{
	//	echo '<div id="border1"><span class="red_dot"></span> <span style="font-size:1em;font-weight:bold;">Conflicted Theme:</span> '. $theme. '<br></div>';
     //   echo "The theme you have that might conflict are: $theme";
//	}	
	

 ?>
	