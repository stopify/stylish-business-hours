<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
function get_time_zone_user($hook) {
    wp_register_script('getUserTime_data', SBH_URL . 'assets/js/businesshourFront.js', array('jquery'), '1.0', true);
    wp_register_style('sbh-list-style', SBH_URL . 'assets/css/style.css');
}

add_action('wp_enqueue_scripts', 'get_time_zone_user');

add_shortcode('stylish_business_hour', 'sbh_get_business_hour_data');

function sbh_get_business_hour_data($atts) {
    wp_enqueue_style('sbh-list-style');
    $a = shortcode_atts(array(
        'id' => ''
            ), $atts);
    $option_name = "stylish_business_hour" . '_' . esc_attr($a['id']);
    wp_enqueue_script('getUserTime_data');
    $data = get_option($option_name);
    $data_array = json_decode($data);
    $hours_array = array();
    $current_day_col = '#' . $data_array->setting->current_day_color_text;
    $holiday_day_col = '#' . $data_array->setting->jscolor_holiday_color_text;
    /*     * *************************Get current weeks all date*************************************** */
    $monday = strtotime("last monday");
    $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;
    $tuesday = strtotime(date("Y-m-d", $monday) . " +1 days");
    $wednesday = strtotime(date("Y-m-d", $monday) . " +2 days");
    $thusday = strtotime(date("Y-m-d", $monday) . " +3 days");
    $friday = strtotime(date("Y-m-d", $monday) . " +4 days");
    $saturday = strtotime(date("Y-m-d", $monday) . " +5 days");
    $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

    $mon = date("Y-m-d", $monday);
    $tue = date("Y-m-d", $tuesday);
    $wed = date("Y-m-d", $wednesday);
    $thu = date("Y-m-d", $thusday);
    $fri = date("Y-m-d", $friday);
    $sat = date("Y-m-d", $saturday);
    $sun = date("Y-m-d", $sunday);
    $current_week_date = array($mon, $tue, $wed, $thu, $fri, $sat, $sun);

    /*     * *************************End get current weeks all date*********************************** */
    $html = '';
    if ($current_day_col != '') {
        ?>
        <style type='text/css'>
            .current_day{
                color:<?php echo $current_day_col ?> !important;
            }
        </style>
        <?php
    }
    if ($holiday_day_col != '') {
        ?>
        <style type='text/css'>
            .holidays{
                color:<?php echo $holiday_day_col ?> !important;
            }
        </style>
        <?php
    }


    $selected_view = $data_array->selected_view;
    $days = $data_array->days;
    $holiday_days = $data_array->spcial_holiday;
    /*     * *************Get holidays day from holidays date********************* */
    $holidays = array();
    foreach ($holiday_days as $key => $val) {
        if (in_array($key, $current_week_date)) {
            $holiday[$key] = date('l', strtotime($key));
        }
    }

    /*     * *************End get holidays day from holidays date***************** */
    $current_day = date('l');
    $html = '';
    foreach ($days as $key => $val) {
        if ($val->status == 'open') {
            $hours_array[] = $val->hourDiff;
        }
    }

    if (!empty($hours_array)) {
        $maximumhours = max($hours_array);
    }
    switch ($selected_view) {

        case 'style_business_1':

            $html = '<div class="sbh_style_business_1 office-hours custom-sbh">' .
                    ' <table class="custom-border">' .
                    '<thead>' .
                    '<th><span class="style_heading listing_type_1"></span></th>' .
                    '</thead>' .
                    '<tbody>';

            foreach ($days as $key => $val) {
                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.= '<tr class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><td><strong>' . $key . '</strong><span>' . $val->open . ' - ' . $val->close . ' <br> ' . $notice . '</span></td></tr>';
                } else {
                    $html.= '<tr class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><td><strong>' . $key . '</strong><span>Closed <br> ' . $notice . '</span></td></tr>';
                }
            }
            $html.='</tbody>' .
                    '</table>' .
                    '</div>';
            break;


        case 'style_business_2':
            $html = '<div class="sbh_style_business_2 second-design  custom-sbh">' .
                    '<table>' .
                    '<tbody>';
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.='<tr class="' . $holidays_class . '">' .
                            '<td class="bottom-border ' . $current_dayclass . '">' . $key . '</td>' .
                            ' <td>' . $val->open . '</td>' .
                            '<td>' . $val->close . '</td>' . $notice .
                            '</tr>';
                } else {
                    $html.='<tr class="' . $holidays_class . '">' .
                            '<td class="bottom-border ' . $current_dayclass . '">' . $key . '</td>' .
                            ' <td colspan="2" class="sbh_style_2_close_btn">Closed</td>' . $notice .
                            '</tr>';
                }
            }
            $html.='</tbody>' .
                    '</table>' .
                    '</div>';
            break;


        case 'style_business_3':
            $html = '<div class="sbh_style_business_3 business-hours style3_img">' .
                    '<div class="business-hours-left"><img src="' . SBH_URL . 'assets/images/icon-watch.png">' .
                    '</div>' .
                    '<div class="business-hours-right style3_heading">' .
                    '<span class="style_heading listing_type_3"></span></br>';
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.= '<tr class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><td><strong class="style3_day" >' . $key . ':</strong><span>' . $val->open . ' - ' . $val->close . '</span>' . $notice . '</td><br></tr>';
                } else {
                    $html.= '<tr class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><td><strong class="style3_day" >' . $key . ':</strong><span class="close">Closed</span><br>' . $notice . '</td></tr>';
                }
            }
            $html.='</div></div>';
            break;


        case 'style_business_4':
            $html = '<div class="sbh_style_business_4 working-time custom-working-sbh">' .
                    '<div class="row"><div class="col-md-2 st_4_heading"><img src="' . SBH_URL . 'assets/images/clock-new.png"></div><div class="col-md-10"><span class="working-time1 listing_type_4"><span class="step-9-icon"></span></span></div></div>' .
                    '<table>' .
                    '<tbody>';
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.= '<tr class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><td class="style_4_sbh"><strong>' . $key . '</strong><span>' . $val->open . ' - ' . $val->close . '</span>' . $notice . '</td></tr>';
                } else {
                    $html.= '<tr class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><td class="style_4_sbh"><strong>' . $key . '</strong><span>Closed</span>' . $notice . '</td></tr>';
                }
            }
            $html.='</tbody>' .
                    '</table>' .
                    '</div>';
            break;

        case 'style_business_5':
            $html = '<div class="sbh_style_business_5 mid-section"><div class="custom-row"><span class="style_heading listing_type_5"></span></div>';
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $hours = (int) $val->hourDiff;
                    $css_per = ($hours * 97) / (int) $maximumhours;
                    if ($maximumhours == $hours) {
                        $css_per = 97;
                    }

                    $html.='<div class="custom-row ' . $current_dayclass . ' ' . $holidays_class . '">' .
                            '<span class="header-title-sbh">' . $key . '</span>' .
                            '<div class="custom-progress-bar">' .
                            '<div class="start-point"><span class="custom-span-left">' . $val->open . '</span></div>' .
                            '<div class="inner-progress" style="width:' . $css_per . '%;background:' . $inner_color . '"></div>' .
                            '<div class="end-point"><span class="custom-float-right">' . $val->close . '</span></div>' .
                            '</div>' .
                            ' </div>' . $notice;
                } else {
                    $html.='<div class="custom-row ' . $current_dayclass . ' ' . $holidays_class . '">' .
                            '<span class="header-title-sbh">' . $key . '</span>' .
                            '<div class="custom-progress-bar">' .
                            // '<div class="start-point"></div>' .
                            '<div class="inner-progress" style="width:0%"></div>' .
                            '</div>' .
                            '<div class="custom-time-info">' .
                            '<span class="custom-float-left">Closed</span>' .
                            '</div>' .
                            ' </div>' . $notice;
                }
            }
            $html.= '</div>' .
                    '<input type="hidden" id="color_value" value="">';
            break;

        case 'style_business_6':
            $html = '<div class="sbh_style_business_6 b-hbay-by-day style6_main">' .
                    '<img src="' . SBH_URL . 'assets/images/clock-new.png"><p><span class="business-hours-bay listing_type_6"><span id="img_business_hour_day"></span></span></p>';
            $index = 0;
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    if ($index % 2 == 0) {
                        $html.='<div class="business-hours-bay-left ' . $current_dayclass . ' ' . $holidays_class . '">' .
                                '<div class="inner-div"><p><strong>' . $key . '</strong><br>' .
                                '<span>' . $val->open . ' - ' . $val->close . '</span></p>' . $notice . '</div>' .
                                '</div>';
                    } else {
                        $html.='<div class="business-hours-bay-right ' . $current_dayclass . ' ' . $holidays_class . '">' .
                                '<div class="inner-div"><p><strong>' . $key . '</strong><br>' .
                                '<span>' . $val->open . ' - ' . $val->close . '</span></p>' . $notice . '</div>' .
                                '</div>';
                    }
                } else {
                    if ($index % 2 == 0) {
                        $html.='<div class="custom-close business-hours-bay-left ' . $current_dayclass . ' ' . $holidays_class . '">' .
                                '<div class="inner-div"><p><strong>' . $key . '</strong><br>' .
                                '<span>Closed</p>' . $notice . '</div>' .
                                '</div>';
                    } else {
                        $html.='<div class="custom-close business-hours-bay-right ' . $current_dayclass . ' ' . $holidays_class . '">' .
                                '<div class="inner-div"><p><strong>' . $key . '</strong><br>' .
                                '<span>closed</p>' . $notice . '</div>' .
                                '</div>';
                    }
                }
                $index++;
            }
            $html.='</div></div>';
            break;

        case 'style_business_7':
            $html = '<div class="sbh_style_business_7 second-design-new">' .
                    '<div class="row style7_head"><div class="col-md-2"><img src="' . SBH_URL . 'assets/images/clock-new.png"></div><div class="col-md-10"><span class="new-bb style_heading listing_type_7"><span id="img_second-design-new"></span><span></span></span></div></div>';
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<lable>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</lable>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.= '<p class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><strong class="style7_day">' . $key . '</strong><span> ' . $val->open . ' - ' . $val->close . ' <br> ' . $notice . '</span>';
                } else {
                    $html.= '<p class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><strong class="style7_day">' . $key . '</strong><span class="sbh_style_7_close_btn"> Closed</span>' . $notice . '</span>';
                }
            }
            $html.='</div>';
            break;


        case 'style_business_8':
            $html = '<div class="sbh_style_business_8 Business-new-8">' .
                    '<div class="style8_container"><span class="new-bb-yellow style_heading listing_type_8"><span></span></span>';

            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);

                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<lable>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</lable>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.= '<p class="bottom-border  ' . $current_dayclass . ' ' . $holidays_class . '"> <strong class="style8_day">' . $key . '</strong><span> ' . $val->open . ' - ' . $val->close . ' <br>' . $notice . '</span>';
                } else {
                    $html.= '<p class="bottom-border ' . $current_dayclass . ' ' . $holidays_class . '"><strong class="style8_day">' . $key . '</strong><span class="sbh_style_8_close_btn"> Closed</span>' . $notice . '</span>';
                }
            }

            $html.='</div></div>';
            break;

        case 'style_business_9':
            $html = '<div class="sbh_style_business_9 bac-business style_9_frontend">' .
                    '<form>' .
                    '<fieldset>';
            foreach ($days as $key => $val) {

                if (isset($key, $holiday) && in_array($key, $holiday)) {
                    $holidays_class = "holidays";
                    $d = array_keys($holiday);
                    $i = 0;
                    foreach ($d as $dd) {
                        $day_name = date("l", strtotime($d[$i]));
                        if ($key == $day_name) {
                            $date = date("l F d,", strtotime($dd));
                            $notice = "<p>Attention: " . $date . " is a holiday and the business hours will be $val->open to $val->close.</p>";
                        }
                        $i++;
                    }
                } else {
                    $holidays_class = "";
                    $notice = "";
                }
                if ($key == $current_day) {
                    $current_dayclass = "current_day";
                } else {
                    $current_dayclass = "";
                }
                if ($val->status == 'open') {
                    $html.='<p class="' . $current_dayclass . ' ' . $holidays_class . '"><strong class="style9_day">' . $key . '</strong><span> ' . $val->open . ' - ' . $val->close . '</span>' . $notice . '</span>';
                } else {
                    $html.='<p class="' . $current_dayclass . ' ' . $holidays_class . '"><strong class="style9_day">' . $key . '</strong><span> Closed</span>' . $notice . '</span>';
                }
            }
            $html.='</fieldset>' .
                    '</form>' .
                    '</div>';
            break;
    }
    echo $html;
    ?>
    <!--Start dynamic css setting-->
    <style>
    <?php if ($data_array->selected_view == 'style_business_1') { ?>
            .sbh_style_business_1.office-hours tr.bottom-border td strong {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_2') { ?>
            .sbh_style_business_2.second-design td.bottom-border {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_3') { ?>
            .sbh_style_business_3 .style3_day {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_4') { ?>
            .sbh_style_business_4.custom-working-sbh td.style_4_sbh strong{
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
            .sbh_style_business_4.custom-working-sbh td.style_4_sbh span{
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_5') { ?>
            .sbh_style_business_5 .custom-row span.header-title-sbh {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_6') { ?>
            .sbh_style_business_6 .business-hours-bay-left p strong, .business-hours-bay-right p strong {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_7') { ?>
            .sbh_style_business_7.second-design-new p.bottom-border strong.style7_day {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_8') { ?>
            .sbh_style_business_8 .style8_container .bottom-border strong.style8_day {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
    <?php if ($data_array->selected_view == 'style_business_9') { ?>
            .sbh_style_business_9 strong.style9_day {
                font-size: <?php echo $data_array->bhp_fontsize; ?> !important;
                font-family: <?php echo $data_array->bhp_fonttype; ?> !important;
            }
    <?php } ?>
        .style_heading{
            color: #<?php echo $data_array->item_icon_color; ?> !important;
            text-transform: uppercase;
        }
        .start-point{
            background: #<?php echo $data_array->item_icon_color; ?> !important;
        }
        .end-point{
            background: #<?php echo $data_array->item_icon_color; ?> !important;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            var title_name = '<?php echo $data_array->list_name; ?>';
            jQuery('.listing_type_1').text(title_name);
            jQuery('.listing_type_3').text(title_name);
            jQuery('.listing_type_4').text(title_name);
            jQuery('.listing_type_5').text(title_name);
            jQuery('.listing_type_6').text(title_name);
            jQuery('.listing_type_7').text(title_name);
            jQuery('.listing_type_8').text(title_name);
            jQuery('.listing_type_9').text(title_name);
        });
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            var color_set = '<?php echo $data_array->item_icon_color; ?>';
            //alert(color_set);
            jQuery("#color_value").val(color_set);
            var color_item = jQuery("#item_icon_color").val();

            /* IE10+ */
            jQuery(".inner-progress").css("background-image", "-ms-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

            /* Mozilla Firefox */
            jQuery(".inner-progress").css("background-image", "-moz-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

            /* Opera */
            jQuery(".inner-progress").css("background-image", "-o-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

            /* Webkit (Safari/Chrome 10) */
            jQuery(".inner-progress").css("background-image", "-webkit-gradient(linear, right top, left top, color-stop(0, #e8e7ec), color-stop(100, #" + color_item + "))");

            /* Webkit (Chrome 11+) */
            jQuery(".inner-progress").css("background-image", "-webkit-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

            /* W3C Markup */
            jQuery(".inner-progress").css("background-image", "linear-gradient(to left, #e8e7ec 0%, #" + color_item + " 100%)");

            jQuery("#item_icon_color").change(function () {
                var color_item = jQuery("#item_icon_color").val();
                /* IE10+ */
                jQuery(".inner-progress").css("background-image", "-ms-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

                /* Mozilla Firefox */
                jQuery(".inner-progress").css("background-image", "-moz-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

                /* Opera */
                jQuery(".inner-progress").css("background-image", "-o-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

                /* Webkit (Safari/Chrome 10) */
                jQuery(".inner-progress").css("background-image", "-webkit-gradient(linear, right top, left top, color-stop(0, #e8e7ec), color-stop(100, #" + color_item + "))");

                /* Webkit (Chrome 11+) */
                jQuery(".inner-progress").css("background-image", "-webkit-linear-gradient(right, #e8e7ec 0%, #" + color_item + " 100%)");

                /* W3C Markup */
                jQuery(".inner-progress").css("background-image", "linear-gradient(to left, #e8e7ec 0%, #" + color_item + " 100%)");
            });
        });
    </script>
    <?php }

