<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if (is_admin()) {

    GLOBAL $wpdb;
    $wp_prefix = $wpdb->prefix;

    // This includes the dbDelta function from WordPress.

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    if ('0.0' == STYLISH_BUSINESS_HOUR_VERSION) {

        //we my do some reset job here, like delete the table
    }

//begin dataase opration
    update_option('business_hours_version', STYLISH_BUSINESS_HOUR_VERSION);
//end dataase opration
}