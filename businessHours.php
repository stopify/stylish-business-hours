<?php
/*
  Plugin Name: Business Hours & Holiday Hours
  Description: a plugin to for displaying Business Hours on web site
  Version: 1.0
  Author: Designful
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define('STYLISH_BUSINESS_HOUR_VERSION', '1.0');
define('SBH_URL', plugin_dir_url(__FILE__));
define('SBH_DIR', dirname(__FILE__));
require_once SBH_DIR . '/admin/businessHourfunction.php';
require_once SBH_DIR . '/shortcode/businesshourFront.php';
require_once SBH_DIR . '/admin/functions/businesshour_ajax.php';
require_once SBH_DIR . '/language_converter.php';

if (is_admin()) {
    // We are in admin mode
    $spl_installed = get_option('stylish_business_hours_version');
    if ($spl_installed != STYLISH_BUSINESS_HOUR_VERSION) {
        include_once( dirname(__FILE__) . '/sbh-install.php' );
    }
}

function sbh_remove_slash_quotes($string) {
    $string = stripslashes($string);
    $string = str_replace('\\\\', '', $string);
    $string = str_replace("\\'", "'", $string);
    $string = str_replace('\\"', '"', $string);
    $string = htmlentities($string);
    return $string;
}

class Stylish_Business_List {

    public function __construct() {
        add_action('init', array($this, 'init'));
        register_activation_hook(__FILE__, array($this, 'activation'));
        register_deactivation_hook(__FILE__, array($this, 'deactivation'));
    }

    function init() {
        
    }

    // save information for new installations
    function activation() {
        
    }

    function deactivation() {
        
    }

}

$stylish_business_list = new Stylish_Business_List();
?>