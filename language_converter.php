<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class SBH_change_lang { 

    public function sbh_change_lang_function($lang,$keyword){
        $language=array();
        
        $language['EN']['save']                                   = "Save";
        $language['EN']['your_shortcode']                         = "Your Shortcode";
        $language['EN']['change_language']                        = "Change Language";
        $language['EN']['admin_setting']                          = "Admin Setting";
        $language['EN']['list_name']                              = "List Name";
        $language['EN']['style']                                  = "Style";
        $language['EN']['font_size']                              = "Font size";
        $language['EN']['current_day_highlight_color']            = "Current-Day Highlight Color";
        $language['EN']['holiday_highlight_color']                = "Holiday Highlight Color";
        $language['EN']['font_type']                              = "Font type";
        $language['EN']['select_icon']                            = "Select icon";
        $language['EN']['regular_business_hours']                 = "Regular Business Hours";
        $language['EN']['select_time_zone']                       = "Select Time-zone";
        $language['EN']['time_format']                            = "Time Format";
        $language['EN']['monday']                                 = "Monday";
        $language['EN']['tuesday']                                = "Tuesday";
        $language['EN']['wednesday']                              = "Wednesday";
        $language['EN']['thursday']                               = "Thursday";
        $language['EN']['friday']                                 = "Friday";
        $language['EN']['saturday']                               = "Saturday";
        $language['EN']['sunday']                                 = "Sunday";
        $language['EN']['holidays_and_special_hours']             = "Holidays and Special Hours";
        $language['EN']['display']                                = "Display";
        $language['EN']['hours_before']                           = "Hours before";
        $language['EN']['remove']                                 = "Remove";
        $language['EN']['hours_after']                            = "Hours after";
        $language['EN']['add']                                    = "Add";
        $language['EN']['close']                                  = "Close";
        $language['EN']['open']                                   = "Open";
        $language['EN']['repeat_yearly']                          = "Repeat Yearly";
        $language['EN']['preview_list']                           = "Preview list";
        $language['EN']['closed']                                 = "CLOSED";
        $language['EN']['select_background_image']                = "Select Background Image";
        
        
        // spanish
        
        $language['SP']['save']                                   = sbh_remove_slash_quotes("Salvar");
        $language['SP']['your_shortcode']                         = sbh_remove_slash_quotes("Su código corto");
        $language['SP']['change_language']                        = sbh_remove_slash_quotes("Cambiar idioma");
        $language['SP']['admin_setting']                          = sbh_remove_slash_quotes("Configuración del administrador");
        $language['SP']['list_name']                              = sbh_remove_slash_quotes("Lista de nombres");
        $language['SP']['style']                                  = sbh_remove_slash_quotes("Estilo");
        $language['SP']['font_size']                              = sbh_remove_slash_quotes("Tamaño de fuente");
        $language['SP']['current_day_highlight_color']            = sbh_remove_slash_quotes("Color de resaltado del día actual");
        $language['SP']['holiday_highlight_color']                = sbh_remove_slash_quotes("Color de resaltado de vacaciones");
        $language['SP']['font_type']                              = sbh_remove_slash_quotes("Tipo de fuente");
        $language['SP']['select_icon']                            = sbh_remove_slash_quotes("Seleccionar ícono");
        $language['SP']['regular_business_hours']                 = sbh_remove_slash_quotes("Horario comercial regular");
        $language['SP']['select_time_zone']                       = sbh_remove_slash_quotes("Selecciona la zona horaria");
        $language['SP']['time_format']                            = sbh_remove_slash_quotes("Formato de tiempo");
        $language['SP']['monday']                                 = sbh_remove_slash_quotes("lunes");
        $language['SP']['tuesday']                                = sbh_remove_slash_quotes("martes");
        $language['SP']['wednesday']                              = sbh_remove_slash_quotes("miércoles");
        $language['SP']['thursday']                               = sbh_remove_slash_quotes("jueves");
        $language['SP']['friday']                                 = sbh_remove_slash_quotes("viernes");
        $language['SP']['saturday']                               = sbh_remove_slash_quotes("sábado");
        $language['SP']['sunday']                                 = sbh_remove_slash_quotes("domingo");
        $language['SP']['holidays_and_special_hours']             = sbh_remove_slash_quotes("Vacaciones y Horas Especiales");
        $language['SP']['display']                                = sbh_remove_slash_quotes("Monitor");
        $language['SP']['hours_before']                           = sbh_remove_slash_quotes("Horas antes");
        $language['SP']['remove']                                 = sbh_remove_slash_quotes("retirar");
        $language['SP']['hours_after']                            = sbh_remove_slash_quotes("Horas despues");
        $language['SP']['add']                                    = sbh_remove_slash_quotes("Añadir");
        $language['SP']['close']                                  = sbh_remove_slash_quotes("Cerca");
        $language['SP']['open']                                   = sbh_remove_slash_quotes("Abierto");
        $language['SP']['repeat_yearly']                          = sbh_remove_slash_quotes("Repita anualmente");
        $language['SP']['preview_list']                           = sbh_remove_slash_quotes("Lista de vista previa");
        $language['SP']['closed']                                 = sbh_remove_slash_quotes("CERRADO");
        $language['SP']['select_background_image']                = sbh_remove_slash_quotes("Seleccionar imagen de fondo");
        
        // french
        
        $language['FR']['save']                                   = sbh_remove_slash_quotes("sauvegarder");
        $language['FR']['your_shortcode']                         = sbh_remove_slash_quotes("Votre Shortcode");
        $language['FR']['change_language']                        = sbh_remove_slash_quotes("Changer de langue");
        $language['FR']['admin_setting']                          = sbh_remove_slash_quotes("Paramètre Admin");
        $language['FR']['list_name']                              = sbh_remove_slash_quotes("Liste de noms");
        $language['FR']['style']                                  = sbh_remove_slash_quotes("Style");
        $language['FR']['font_size']                              = sbh_remove_slash_quotes("Taille de police");
        $language['FR']['current_day_highlight_color']            = sbh_remove_slash_quotes("Couleur du jour actuel");
        $language['FR']['holiday_highlight_color']                = sbh_remove_slash_quotes("Couleur des fêtes");
        $language['FR']['font_type']                              = sbh_remove_slash_quotes("Type de police");
        $language['FR']['select_icon']                            = sbh_remove_slash_quotes("Icône de sélection");
        $language['FR']['regular_business_hours']                 = sbh_remove_slash_quotes("Heures d'ouverture régulières");
        $language['FR']['select_time_zone']                       = sbh_remove_slash_quotes("Sélectionnez le fuseau horaire");
        $language['FR']['time_format']                            = sbh_remove_slash_quotes("Format de l'heure");
        $language['FR']['monday']                                 = sbh_remove_slash_quotes("Lundi");
        $language['FR']['tuesday']                                = sbh_remove_slash_quotes("Mardi");
        $language['FR']['wednesday']                              = sbh_remove_slash_quotes("Mercredi");
        $language['FR']['thursday']                               = sbh_remove_slash_quotes("Jeudi");
        $language['FR']['friday']                                 = sbh_remove_slash_quotes("Vendredi");
        $language['FR']['saturday']                               = sbh_remove_slash_quotes("samedi");
        $language['FR']['sunday']                                 = sbh_remove_slash_quotes("dimanche");
        $language['FR']['holidays_and_special_hours']             = sbh_remove_slash_quotes("Vacances et Heures Spéciales");
        $language['FR']['display']                                = sbh_remove_slash_quotes("Afficher");
        $language['FR']['hours_before']                           = sbh_remove_slash_quotes("Heures avant");
        $language['FR']['remove']                                 = sbh_remove_slash_quotes("Retirer");
        $language['FR']['hours_after']                            = sbh_remove_slash_quotes("Heures après");
        $language['FR']['add']                                    = sbh_remove_slash_quotes("Ajouter");
        $language['FR']['close']                                  = sbh_remove_slash_quotes("Fermer");
        $language['FR']['open']                                   = sbh_remove_slash_quotes("Ouvrir");
        $language['FR']['repeat_yearly']                          = sbh_remove_slash_quotes("Répéter chaque année");
        $language['FR']['preview_list']                           = sbh_remove_slash_quotes("Liste de prévisualisation");
        $language['FR']['closed']                                 = sbh_remove_slash_quotes("FERMÉ");
        $language['FR']['select_background_image']                = sbh_remove_slash_quotes("Sélectionner une image de fond");
        
        // dutch
        
        $language['DE']['save']                                   = sbh_remove_slash_quotes("Opslaan");
        $language['DE']['your_shortcode']                         = sbh_remove_slash_quotes("Uw shortcode");
        $language['DE']['change_language']                        = sbh_remove_slash_quotes("Wijzig taal");
        $language['DE']['admin_setting']                          = sbh_remove_slash_quotes("Beheerinstelling");
        $language['DE']['list_name']                              = sbh_remove_slash_quotes("Lijstnaam");
        $language['DE']['style']                                  = sbh_remove_slash_quotes("Stijl");
        $language['DE']['font_size']                              = sbh_remove_slash_quotes("Lettertypegrootte");
        $language['DE']['current_day_highlight_color']            = sbh_remove_slash_quotes("Highlight-kleur huidige dag");
        $language['DE']['holiday_highlight_color']                = sbh_remove_slash_quotes("Vakantie markeer kleur");
        $language['DE']['font_type']                              = sbh_remove_slash_quotes("Lettertype");
        $language['DE']['select_icon']                            = sbh_remove_slash_quotes("Selecteer een pictogram");
        $language['DE']['regular_business_hours']                 = sbh_remove_slash_quotes("Reguliere openingstijden");
        $language['DE']['select_time_zone']                       = sbh_remove_slash_quotes("Selecteer Tijdzone");
        $language['DE']['time_format']                            = sbh_remove_slash_quotes("Tijd formaat");
        $language['DE']['monday']                                 = sbh_remove_slash_quotes("maandag");
        $language['DE']['tuesday']                                = sbh_remove_slash_quotes("dinsdag");
        $language['DE']['wednesday']                              = sbh_remove_slash_quotes("woensdag");
        $language['DE']['thursday']                               = sbh_remove_slash_quotes("donderdag");
        $language['DE']['friday']                                 = sbh_remove_slash_quotes("vrijdag");
        $language['DE']['saturday']                               = sbh_remove_slash_quotes("zaterdag");
        $language['DE']['sunday']                                 = sbh_remove_slash_quotes("zondag");
        $language['DE']['holidays_and_special_hours']             = sbh_remove_slash_quotes("Feestdagen en speciale uren");
        $language['DE']['display']                                = sbh_remove_slash_quotes("tonen");
        $language['DE']['hours_before']                           = sbh_remove_slash_quotes("Uren eerder");
        $language['DE']['remove']                                 = sbh_remove_slash_quotes("Verwijderen");
        $language['DE']['hours_after']                            = sbh_remove_slash_quotes("Uren erna");
        $language['DE']['add']                                    = sbh_remove_slash_quotes("Toevoegen");
        $language['DE']['close']                                  = sbh_remove_slash_quotes("Dichtbij");
        $language['DE']['open']                                   = sbh_remove_slash_quotes("Open");
        $language['DE']['repeat_yearly']                          = sbh_remove_slash_quotes("Herhaal elk jaar");
        $language['DE']['preview_list']                           = sbh_remove_slash_quotes("Voorbeeldlijst");
        $language['DE']['closed']                                 = sbh_remove_slash_quotes("GESLOTEN");
        $language['DE']['select_background_image']                = sbh_remove_slash_quotes("Selecteer achtergrondafbeelding");
        
        
        
        // return output
        
        return $language[$lang][$keyword];
        
        
    }
    
   
}
?>