<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'list';
$id  = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
switch ($action) {
    case 'delete':
        $ids = isset($_REQUEST['ids']) ? $_REQUEST['ids'] : null;
        if (!empty($ids)) {
            foreach ($ids as $key => $id) {
                sbh_delete_tabs_by_id($id);
            }
        } else if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            sbh_delete_tabs_by_id($id);
        }
    default:
        $template = dirname(__FILE__) . '/views/tabs-list.php';
        break;
}



if (!class_exists('WP_List_Table')) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
    include_once dirname(__FILE__) . '/logo-footer.php';
}


function sbh_get_all_tabs()
{
    
    global $wpdb;
    
    $prefix       = 'stylish_business_hour_';
    $option_names = $wpdb->get_results("SELECT * FROM $wpdb->options WHERE option_name LIKE '$prefix%'");
    
    $cats_data = array();
    foreach ($option_names as $opt) {
        $option = get_option($opt->option_name);
		$id_data = explode('_',$opt->option_name);
        
        $cat                          = new stdClass();
        $cat->id                      = $opt->option_name;
        $cat->shortcode               = '[stylish_business_hour id="'.$id_data[count($id_data) - 1].'"]';
        $option_value                 = $opt->option_value;
        $listname                     = json_decode($option_value);
        $cat->list_name               = $listname->list_name;
        $cats_data[$opt->option_name] = $cat;
        
        // delete_option($opt->option_name);
    }
    return $cats_data;
}
function sbh_delete_tabs_by_id($id)
{
    $option_name = $id;
    delete_option($option_name);
}
function sbh_get_tabs_count()
{
    return count(sbh_get_all_tabs());
}
class Stylish_Business_Hour_List extends \WP_List_Table
{
    
    function __construct()
    {
        parent::__construct(array(
            'singular' => 'List',
            'plural' => 'Lists',
            'ajax' => false
        ));
    }
    
    function get_table_classes()
    {
        return array(
            'widefat',
            'fixed',
            'striped',
            $this->_args['plural']
        );
    }
    
    function no_items()
    {
        _e('No Lists Found', 'sbh');
    }
    
    
    function column_default($item, $column_name)
    {
        
        switch ($column_name) {
            case 'id':
                return $item->id;
            
            case 'list_name':
                return $item->list_name;
            
            case 'shortcode':
                return $item->shortcode;
            default:
                return isset($item->$column_name) ? $item->$column_name : '';
        }
    }
    
    /**
     * Get the column names
     *
     * @return array
     */
    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'id' => __('List Name', 'sbh'),
            'shortcode' => __('Shortcode', 'sbh')
        );
        
        return $columns;
    }
    
    /**
     * Render the designation name column
     *
     * @param  object  $item
     *
     * @return string
     */
    function column_id($item)
    {
        
        $actions           = array();
        $actions['edit']   = sprintf('<a href="%s" data-id="%d" title="%s">%s</a>', admin_url('admin.php?page=business_hour_page_new&action=edit&id=' . $item->id), $item->id, __('Edit this item', 'sbh'), __('Edit', 'sbh'));
        $actions['delete'] = sprintf('<a href="%s" class="submitdelete" data-id="%d" title="%s">%s</a>', admin_url('admin.php?page=business_hour_listing&action=delete&id=' . $item->id), $item->id, __('Delete this item', 'sbh'), __('Delete', 'sbh'));
        
        return sprintf('<a href="%1$s"><strong>%2$s</strong></a> %3$s', admin_url('admin.php?page=business_hour_page_new&action=edit&id=' . $item->id), $item->list_name, $this->row_actions($actions));
    }
    
    function column_refer($item)
    {
        return sprintf('<a href="%s" data-id="%d" title="%s">%s</a>', admin_url('admin.php?page=business_hour_page_new&action=readonly&id=' . $item->id), $item->id, __('#' . $item->id, 'c9s'), __('#' . $item->id, 'c9s'));
    }
    
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'name' => array(
                'name',
                true
            )
        );
        
        return $sortable_columns;
    }
    
    function get_bulk_actions()
    {
        $actions = array(
            'delete' => __('Delete Selected Items', 'sbh')
        );
        return $actions;
    }
    
    
    function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="ids[]" value="%s" />', $item->id);
    }
    
    public function get_views_()
    {
        $status_links = array();
        $base_link    = admin_url('admin.php?page=sample-page');
        
        foreach ($this->counts as $key => $value) {
            $class              = ($key == $this->page_status) ? 'current' : 'status-' . $key;
            $status_links[$key] = sprintf('<a href="%s" class="%s">%s <span class="count">(%s)</span></a>', add_query_arg(array(
                'status' => $key
            ), $base_link), $class, $value['label'], $value['count']);
        }
        
        return $status_links;
    }
    
    
    function prepare_items()
    {
        
        $columns               = $this->get_columns();
        $hidden                = array();
        $sortable              = $this->get_sortable_columns();
        $this->_column_headers = array(
            $columns,
            $hidden,
            $sortable
        );
        
        $per_page          = 20;
        $current_page      = $this->get_pagenum();
        $offset            = ($current_page - 1) * $per_page;
        $this->page_status = isset($_GET['status']) ? sanitize_text_field($_GET['status']) : '2';
        $search            = isset($_REQUEST['s']) ? sanitize_text_field($_REQUEST['s']) : '';
        
        // only ncessary because we have sample data
        $args = array(
            'offset' => $offset,
            'number' => $per_page,
            'search' => '*' . $search . '*'
        );
        
        if (isset($_REQUEST['orderby']) && isset($_REQUEST['order'])) {
            $args['orderby'] = $_REQUEST['orderby'];
            $args['order']   = $_REQUEST['order'];
        }
        
        $this->items = sbh_get_all_tabs($args);
        
        $this->set_pagination_args(array(
            'total_items' => sbh_get_tabs_count(),
            'per_page' => $per_page
        ));
    }
    
}

?>