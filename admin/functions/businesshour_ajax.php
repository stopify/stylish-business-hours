<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
function sbh_get_option_prefix()
{
    return 'stylish_business_hour_';
}

function generate_shortcode($bhp_fontsize, $bhp_fonttype, $bs_custom_timeformat, $bs_custom_display_type, $shortcode, $days, $selected_view, $holiday_days, $spcial_holiday, $list_name, $shortcode_ID, $selected_icon,$hour_after,$hour_before,$lang,$item_icon_color)
{
    global $wpdb;
    $setting = array();
    if (!empty($shortcode)) {
        $setting['setting']                = $shortcode;
        $setting['days']                   = $days;
        $setting['bs_custom_display_type'] = $bs_custom_display_type;
        $setting['selected_view']          = $selected_view;
        $setting['holidays']               = $holiday_days;
        $setting['bhp_fontsize']           = $bhp_fontsize;
        $setting['bhp_fonttype']           = $bhp_fonttype;
        $setting['spcial_holiday']         = $spcial_holiday;
        $setting['bs_custom_timeformat']   = $bs_custom_timeformat;
        $setting['list_name']              = $list_name;
        $setting['select_bhp_icon']        = $selected_icon;
        $setting['hour_after']             = $hour_after;
        $setting['hour_before']            = $hour_before;
        $setting['lang']                   = $lang;
        $setting['item_icon_color']        = $item_icon_color;
        $setting_data                      = json_encode($setting);
        $prefix                            = sbh_get_option_prefix();

        $option_names= $wpdb->get_results("SELECT option_name FROM $wpdb->options WHERE option_name LIKE '$prefix%'");
        if (!empty($option_names)):
            $time= time();
            $option_name_generate = 'stylish_business_hour_' . $time;
            if ($shortcode_ID != '') {
                $option_name_generate = $shortcode_ID;
            }
            update_option($option_name_generate, $setting_data);
            $id_data = explode('_',$option_name_generate);
            $opt_name='[stylish_business_hour id="'.$id_data[count($id_data) - 1].'"]';
            return $opt_name;
        else:
            $time = time();
            $option_name_generate = 'stylish_business_hour_' . $time;
            update_option($option_name_generate, $setting_data);
            $id_data = explode('_',$option_name_generate);
            $opt_name='[stylish_business_hour id="'.$id_data[count($id_data) - 1].'"]';
            return $opt_name;
        endif;
    }
}


function set_business_hour_data()
{
    
    $shortcode_ID           = '';
    $shortcode              = array();
    $days                   = array();
    $holiday_days           = array();
    $holiday_data           = array();
    $spcial_holiday         = array();
    $data                   = $_REQUEST['requried_data'];
    $list_name              = $_REQUEST['list_name'];
    $bhp_fontsize           = $_REQUEST['bhp_fontsize'];
    $selected_icon          = $_REQUEST['selected_icon'];
    $bhp_fonttype           = $_REQUEST['bhp_fonttype'];
    $bs_custom_timeformat   = $_REQUEST['bs_custom_timeformat'];
    $selected_view          = $_REQUEST['selected_view'];
    $bs_custom_display_type = $_REQUEST['bs_custom_display_type'];
    $holidays               = $_REQUEST['holidays'];
    $hour_after             = $_REQUEST['hour_after'];
    $hour_before            = $_REQUEST['hour_before'];
    $lang                   = $_REQUEST['lang'];
    $item_icon_color        = $_REQUEST['item_icon_color'];
    foreach ($holidays as $data2) {
        $holiday_days[] = $data2['holiday'];
    }
    foreach ($data as $d1) {
        $shortcode[$d1['fields']] = $d1['values'];
    }
    foreach ($_REQUEST['days_timing'] as $value) {
        if ($value['status'] == 'open') {
            $days[$value['day']] = array(
                'open' => $value['opentime'],
                'close' => $value['closetime'],
                'status'=>'open',
                'hourDiff'=>$value['hourDiff']
            );
        } else {
            $days[$value['day']] = array(
                 'status'=>'close'
            );
        }
    }
    foreach ($_REQUEST['special_holiday'] as $key1 => $value1) {
        if ($value1['holiday_status'] == 'open') {
            $spcial_holiday[$value1['holiday_date']] = array(
                'open' => $value1['special_time_picker_open'],
                'close' => $value1['special_time_picker_close'],
                'holiday_title' => $value1['holiday_title'],
                'holiday_status' => $value1['holiday_status']
            );
        } else {
            $spcial_holiday[$value1['holiday_date']] = array(
                'holiday_title' => $value1['holiday_title'],
                'holiday_status' => $value1['holiday_status']
            );
        }
    }
    if ($_REQUEST['shortcode_ID'] != '') {
        $shortcode_ID = $_REQUEST['shortcode_ID'];
    }
    echo $return_shortcode = generate_shortcode($bhp_fontsize, $bhp_fonttype, $bs_custom_timeformat, $bs_custom_display_type, $shortcode, $days, $selected_view, $holiday_days, $spcial_holiday, $list_name, $shortcode_ID, $selected_icon,$hour_after,$hour_before,$lang,$item_icon_color);
    die(0);
}
add_action("wp_ajax_set_business_hour_data", "set_business_hour_data");
add_action("wp_ajax_nopriv_set_business_hour_data", "set_business_hour_data");
?>