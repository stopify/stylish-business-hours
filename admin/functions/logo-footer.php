<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-logo-section">
        <div class="inner-footer-logo-section">  
            <h4>Designed by</h4>
            <a href="#" class="sbh-footer logo">
                <img src="<?php echo SBH_URL . 'assets/images/logo.png'; ?>" class="img-responsive1" alt="Image" >
            </a>
       	</div>
    </div>
</div>
<style type="text/css">
    .sbh-header{
        display: inline-block;
    }
    .sbh-header.logo{
        position: relative;
        top: -20px;
    }
    img.img-responsive1 {
        max-width: 100%;
        height: auto;
    }
    .sbh-header{
        display: inline-block;
    }
    .sbh-header.logo{
        position: relative;
        top: -20px;
    }
    img.img-responsive1 {
        max-width: 100%;
        height: auto;
    }
    .inner-footer-logo-section h4 {
        float: left;
        margin-right: 16px;
    }
    .inner-footer-logo-section {
        float: right;
    }
    .inner-footer-logo-section {
        margin-top: 26px;
    }
    .inner-footer-logo-section img {
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
    }

    .inner-footer-logo-section img {
        width: 47%;
    }
    .inner-footer-logo-section {
        width: 200px;
    }
</style>
