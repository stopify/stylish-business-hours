<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
function get_option_data()
{
    global $wpdb;
	$stylish_id=$_REQUEST['stylish_id'];
    $result       = '';
    $opt_name       = $stylish_id;
    $option_names = $wpdb->get_results("SELECT * FROM $wpdb->options WHERE option_name = '$opt_name'");
    if (!empty($option_names)):
        foreach ($option_names as $opt):
            $values           = $opt->option_value;
            $opt_name         = $opt->option_name;
            $id_data = explode('_',$opt->option_name);
            $opt_name='[stylish_business_hour id="'.$id_data[count($id_data) - 1].'"]';
            $result           = compact('values', 'opt_name');
            $result['status'] = 'ok';
        endforeach;
    else:
        $result['status'] = 'false';
    endif;
    echo json_encode($result);
    die(0);
}
add_action("wp_ajax_get_option_data", "get_option_data");
add_action("wp_ajax_nopriv_get_option_data", "get_option_data");
?>