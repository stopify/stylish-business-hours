<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
require_once dirname(__FILE__) . '/class-settings.php';
include dirname(__FILE__) . '/functions/businesshour_option_data.php';

function wpdocs_register_my_custom_menu_page_business_hour() {
    $icon_url = SBH_URL . '/assets/images/admin_icon.png';
    add_menu_page(
            __('Stylish Business Hours', 'sbh'), __('Stylish Business Hours', 'sbh'), 'manage_options', 'business_hour_listing', 'business_hour_listing_page', $icon_url, 99
    );
    add_submenu_page('business_hour_listing', __('All Lists', 'sbh'), __('All Lists', 'sbh'), 'manage_options', 'business_hour_listing', 'business_hour_listing_page');
    add_submenu_page('business_hour_listing', __('Add New List', 'sbh'), __('Add New List', 'sbh'), 'manage_options', 'business_hour_page_new', 'my_custom_menu_page_business_hour');
    add_submenu_page('business_hour_listing', __('Diagnostic', 'sbh'), __('SBH Diagnostic', 'sbh'), 'manage_options', 'business_hour_page_diagnostic', 'business_hour_page_diagnostic');
}

add_action('admin_menu', 'wpdocs_register_my_custom_menu_page_business_hour');

function business_hour_page_diagnostic(){
    require_once SBH_DIR . '/shortcode/sbh-diagnostic.php';
}

function my_custom_menu_page_business_hour() {

    wp_enqueue_style('businessHour_admin_style');
    wp_enqueue_style('jstimepick_style');
    wp_enqueue_script('jscolorpick');
    wp_enqueue_script('jslibrary');
    require_once SBH_DIR . '/admin/view/businessHourView.php';
    wp_enqueue_script('jstimepick');
    wp_enqueue_script('businessHour_js');
    wp_enqueue_script('moment');
}

function business_hour_listing_page() {
    wp_enqueue_style('dataTable_style');
    require_once dirname(__FILE__) . '/view/businessHourList.php';
    wp_enqueue_script('jslibrary');
    wp_enqueue_script('dataTable_scripts');
    wp_enqueue_script('business_hour_list_js');
}

function stylish_business_hour_settings() {

    require_once dirname(__FILE__) . '/view/businessHourSetting.php';
}

function sbh_js_css_enqueue_scripts($hook) {
    wp_register_style('dataTable_style', SBH_URL . 'assets/lib/DataTables/css/jquery.dataTables.min.css');
    wp_register_style('jstimepick_style', SBH_URL . 'assets/css/jquery.timepicker.min.css');
    wp_register_style('businessHour_admin_style', SBH_URL . 'assets/css/admin_style.css');
    wp_register_script('businessHour_js', SBH_URL . 'assets/js/businesshour.js', array('jquery'), '1.0', true);
    wp_register_script('jscolorpick', SBH_URL . 'assets/js/jscolor.js', array('jquery'), '1.0', true);
    wp_register_script('jstimepick', SBH_URL . 'assets/js/jquery.timepicker.min.js', array('jquery'), '1.0', true);
    wp_register_script('business_hour_list_js', SBH_URL . 'assets/js/business_hour_list.js', array('jquery'), '1.0', true);
    wp_register_script('dataTable_scripts', SBH_URL . 'assets/lib/DataTables/js/jquery.dataTables.min.js', array('jquery'), '1.0', true);
    wp_register_script('moment', SBH_URL . 'assets/js/moment.js', array('jquery'), '1.0', true);
}

add_action('wp_enqueue_scripts', 'sbh_js_css_enqueue_scripts');
add_action('admin_enqueue_scripts', 'sbh_js_css_enqueue_scripts');
?>