<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
//require_once( 'wp-license-manager-client/class-wp-license-manager-client.php' );

include_once SBH_DIR . '/admin/functions/logo-header.php';


define('SBH_SPECIAL_SECRET_KEY', '5421048138b321.90068894');

define('SBH_LICENSE_SERVER_URL', 'http://designful.ca'); 

define('SBH_ITEM_REFERENCE', 'stylish-business-hour');

add_action('admin_menu', 'slm_sbh_license_menu');

function slm_sbh_license_menu() {
    add_options_page('SBH License Activation Menu', 'SBH License', 'manage_options', __FILE__, 'sbh_license_management_page');
}
?>
<h1>Stylish Business Hours Settings</h1>
    <form action="options.php" method="post">
        <table class="form-table">
            <tr>
                <th style="width:100px;"><label for="sbh_license_key">License Key:</label></th>
                <td ><input type="password" type="text" id="sbh_license_key" name="sbh_license_key"  value="<?php echo get_option('sbh_license_key'); ?>" ></td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" name="activate_license" value="Activate" class="button-primary" />
            <input type="submit" name="deactivate_license" value="Deactivate" class="button" />
        </p>
    </form>
    
 <?php
 
                function sbh_license_management_page() {
                echo '<div class="wrap">';
                echo '<h2>SBH License Management</h2>';
                
                /*** License activate button was clicked ***/
                if (isset($_REQUEST['activate_license'])) {
                    $license_key = $_REQUEST['sbh_license_key'];
                    
                    // API query parameters
                    $api_params = array(
                        'slm_action' => 'slm_activate',
                        'secret_key' => SBH_SPECIAL_SECRET_KEY,
                        'license_key' => $license_key,
                        'registered_domain' => $_SERVER['SERVER_NAME'],
                        'item_reference' => urlencode(SBH_ITEM_REFERENCE),
                    );
        // Send query to the license manager server
                $query = esc_url(add_query_arg($api_params, SBH_LICENSE_SERVER_URL));
                $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));
                // Check for error in the response
                if (is_wp_error($response)){
                    echo "Unexpected Error! The query returned with an error.";
                }
                //var_dump($response);//uncomment it if you want to look at the full response
                
        //License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));
        
 
        //var_dump($license_data);  //uncomment it to look at the data
        
                if($license_data->result == 'success'){ //Success was returned for the license activation
                    
                    //Uncomment the followng line to see the message that returned from the license server
                    echo '<br />The following message was returned from the server: '.$license_data->message;
                    
                    //Save the license key in the options table
                    update_option('sbh_license_key', $license_key); 
                    echo $license_key;
                }
                else{
                    //Show error to the user. Probably entered incorrect license key.
                    
                    //Uncomment the followng line to see the message that returned from the license server
                    echo '<br />The following message was returned from the server: '.$license_data->message;
                }
            }
    /*** End of license activation ***/
    
    /*** License activate button was clicked ***/
        if (isset($_REQUEST['deactivate_license'])) {
            $license_key = $_REQUEST['sbh_license_key'];
            // API query parameters
            $api_params = array(
                'slm_action' => 'slm_deactivate',
                'secret_key' => SBH_SPECIAL_SECRET_KEY,
                'license_key' => $license_key,
                'registered_domain' => $_SERVER['SERVER_NAME'],
                'item_reference' => urlencode(SBH_ITEM_REFERENCE),
            );
            
            // Send query to the license manager server
            $query = esc_url(add_query_arg($api_params, SBH_LICENSE_SERVER_URL));
            $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));
            // Check for error in the response
            if (is_wp_error($response)){
                
                return false;
            }

        $license_data = json_decode(wp_remote_retrieve_body($response));
        
        return $license_data;
        
        
        if($license_data->result == 'success'){ //Success was returned for the license activation
            
            //Uncomment the followng line to see the message that returned from the license server
            echo '<br />The following message was returned from the server: '.$license_data->message;
            
            //Remove the licensse key from the options table. It will need to be activated again.
            update_option('sbh_license_key', '');
        }
        else{
            //Show error to the user. Probably entered incorrect license key.
            
            //Uncomment the followng line to see the message that returned from the license server
            echo '<br />The following message was returned from the server: '.$license_data->message;
        }
        
    }
    /*** End of sample license deactivation ***/
    
    ?>
    
    <?php
    
    echo '</div>';
}
