<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
require SBH_DIR . '/admin/functions/business_hour_listing.php';
?>
<div class="wrap">
    <h2><?php _e('Business Hour Plugin List', 'sbl'); ?> <a href="<?php echo admin_url('admin.php?page=business_hour_page_new'); ?>" class="add-new-h2"><?php _e('Add New', 'sbh'); ?></a></h2>
    <img src="<?php echo SBH_URL . 'assets/images/Stylish-Business-Hours-Logo1.png'; ?>" style="width: 300px;"/>
    <form method="post">
        <input type="hidden" name="page" value="ttest_list_table">
        <?php
        $list_table = new Stylish_Business_Hour_List();
        $list_table->prepare_items();
        $list_table->search_box('search', 's');
        $list_table->display();
        ?>
    </form>
</div>
<?php
require SBH_DIR . '/admin/functions/logo-footer.php';
?>