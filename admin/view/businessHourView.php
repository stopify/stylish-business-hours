<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<link rel='stylesheet' id='sbh-list-style-css'  href="<?php echo SBH_URL . 'assets/css/style.css'; ?>" type='text/css' media='all' />
<?php
$url = "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyCMJoq5tHWewz1IYpOrlfXHRekweGOTEKk";
$result = json_decode(file_get_contents($url));
$font_list = array();
foreach ($result->items as $font) {
    $font_list[] = [
        'font_name' => $font->family,
        'category' => $font->category,
        'variants' => implode(', ', $font->variants),
    ];
}
require_once SBH_DIR . '/admin/functions/wp_page_function.php';

// get license activation option
$opt = get_option('sbh_license_return');

// create object of langues class

$timezone = array();
$timezone = generate_timezone_list();
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
$id_shortcode = explode('_', $id);

$datas = json_decode(get_option($id));
$selected_icon = $datas->select_bhp_icon;
$days = $datas->days;

$lang_obj = new SBH_change_lang;

if ($datas->selected_view != '' || $datas->selected_view == 'style_business_3' || $datas->selected_view == 'style_business_4' || $datas->selected_view == 'style_business_9') {
    ?>
    <style>
        .display_alldays{
            display:none ;
        }
    </style>
    <?php
}
if ($datas->selected_view == 'style_business_1' || $datas->selected_view == 'style_business_2' || $datas->selected_view == 'style_business_5' || $datas->selected_view == 'style_business_6' || $datas->selected_view == 'style_business_7' || $datas->selected_view == 'style_business_8') {
    ?>
    <style>
        .display_weekday{
            display:none ;
        }
        .display_alldays{
            display:table-row ;
        }
    </style>
    <?php
}
$item_icon_color = isset($datas->item_icon_color) ? $datas->item_icon_color : '000000';
if (isset($_REQUEST['lang'])) {
    $save = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'save');
    $your_shortcode = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'your_shortcode');
    $change_language = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'change_language');
    $admin_setting = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'admin_setting');
    $list_name = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'list_name');
    $style = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'style');
    $font_size = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'font_size');
    $current_day_highlight_color = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'current_day_highlight_color');
    $holiday_highlight_color = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'holiday_highlight_color');
    $font_type = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'font_type');
    $select_icon = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'select_icon');
    $regular_business_hours = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'regular_business_hours');
    $select_time_zone = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'select_time_zone');
    $time_format = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'time_format');
    $monday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'monday');
    $tuesday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'tuesday');
    $wednesday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'wednesday');
    $thursday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'thursday');
    $friday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'friday');
    $saturday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'saturday');
    $sunday = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'sunday');
    $holidays_and_special_hours = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'holidays_and_special_hours');
    $display = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'display');
    $hours_before = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'hours_before');
    $remove = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'remove');
    $hours_after = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'hours_after');
    $add = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'add');
    $close = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'close');
    $open = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'open');
    $repeat_yearly = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'repeat_yearly');
    $preview_list = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'preview_list');
    $closed = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'closed');
    $select_background_image = $lang_obj->sbh_change_lang_function($_REQUEST['lang'], 'select_background_image');
} else {
    if ($datas->lang != '') {
        $save = $lang_obj->sbh_change_lang_function($datas->lang, 'save');
        $your_shortcode = $lang_obj->sbh_change_lang_function($datas->lang, 'your_shortcode');
        $change_language = $lang_obj->sbh_change_lang_function($datas->lang, 'change_language');
        $admin_setting = $lang_obj->sbh_change_lang_function($datas->lang, 'admin_setting');
        $list_name = $lang_obj->sbh_change_lang_function($datas->lang, 'list_name');
        $style = $lang_obj->sbh_change_lang_function($datas->lang, 'style');
        $font_size = $lang_obj->sbh_change_lang_function($datas->lang, 'font_size');
        $current_day_highlight_color = $lang_obj->sbh_change_lang_function($datas->lang, 'current_day_highlight_color');
        $holiday_highlight_color = $lang_obj->sbh_change_lang_function($datas->lang, 'holiday_highlight_color');
        $font_type = $lang_obj->sbh_change_lang_function($datas->lang, 'font_type');
        $select_icon = $lang_obj->sbh_change_lang_function($datas->lang, 'select_icon');
        $regular_business_hours = $lang_obj->sbh_change_lang_function($datas->lang, 'regular_business_hours');
        $select_time_zone = $lang_obj->sbh_change_lang_function($datas->lang, 'select_time_zone');
        $time_format = $lang_obj->sbh_change_lang_function($datas->lang, 'time_format');
        $monday = $lang_obj->sbh_change_lang_function($datas->lang, 'monday');
        $tuesday = $lang_obj->sbh_change_lang_function($datas->lang, 'tuesday');
        $wednesday = $lang_obj->sbh_change_lang_function($datas->lang, 'wednesday');
        $thursday = $lang_obj->sbh_change_lang_function($datas->lang, 'thursday');
        $friday = $lang_obj->sbh_change_lang_function($datas->lang, 'friday');
        $saturday = $lang_obj->sbh_change_lang_function($datas->lang, 'saturday');
        $sunday = $lang_obj->sbh_change_lang_function($datas->lang, 'sunday');
        $holidays_and_special_hours = $lang_obj->sbh_change_lang_function($datas->lang, 'holidays_and_special_hours');
        $display = $lang_obj->sbh_change_lang_function($datas->lang, 'display');
        $hours_before = $lang_obj->sbh_change_lang_function($datas->lang, 'hours_before');
        $remove = $lang_obj->sbh_change_lang_function($datas->lang, 'remove');
        $hours_after = $lang_obj->sbh_change_lang_function($datas->lang, 'hours_after');
        $add = $lang_obj->sbh_change_lang_function($datas->lang, 'add');
        $close = $lang_obj->sbh_change_lang_function($datas->lang, 'close');
        $open = $lang_obj->sbh_change_lang_function($datas->lang, 'open');
        $repeat_yearly = $lang_obj->sbh_change_lang_function($datas->lang, 'repeat_yearly');
        $preview_list = $lang_obj->sbh_change_lang_function($datas->lang, 'preview_list');
        $closed = $lang_obj->sbh_change_lang_function($datas->lang, 'closed');
        $select_background_image = $lang_obj->sbh_change_lang_function($datas->lang, 'select_background_image');
    } else {
        $save = $lang_obj->sbh_change_lang_function('EN', 'save');
        $your_shortcode = $lang_obj->sbh_change_lang_function('EN', 'your_shortcode');
        $change_language = $lang_obj->sbh_change_lang_function('EN', 'change_language');
        $admin_setting = $lang_obj->sbh_change_lang_function('EN', 'admin_setting');
        $list_name = $lang_obj->sbh_change_lang_function('EN', 'list_name');
        $style = $lang_obj->sbh_change_lang_function('EN', 'style');
        $font_size = $lang_obj->sbh_change_lang_function('EN', 'font_size');
        $current_day_highlight_color = $lang_obj->sbh_change_lang_function('EN', 'current_day_highlight_color');
        $holiday_highlight_color = $lang_obj->sbh_change_lang_function('EN', 'holiday_highlight_color');
        $font_type = $lang_obj->sbh_change_lang_function('EN', 'font_type');
        $select_icon = $lang_obj->sbh_change_lang_function('EN', 'select_icon');
        $regular_business_hours = $lang_obj->sbh_change_lang_function('EN', 'regular_business_hours');
        $select_time_zone = $lang_obj->sbh_change_lang_function('EN', 'select_time_zone');
        $time_format = $lang_obj->sbh_change_lang_function('EN', 'time_format');
        $monday = $lang_obj->sbh_change_lang_function('EN', 'monday');
        $tuesday = $lang_obj->sbh_change_lang_function('EN', 'tuesday');
        $wednesday = $lang_obj->sbh_change_lang_function('EN', 'wednesday');
        $thursday = $lang_obj->sbh_change_lang_function('EN', 'thursday');
        $friday = $lang_obj->sbh_change_lang_function('EN', 'friday');
        $saturday = $lang_obj->sbh_change_lang_function('EN', 'saturday');
        $sunday = $lang_obj->sbh_change_lang_function('EN', 'sunday');
        $holidays_and_special_hours = $lang_obj->sbh_change_lang_function('EN', 'holidays_and_special_hours');
        $display = $lang_obj->sbh_change_lang_function('EN', 'display');
        $hours_before = $lang_obj->sbh_change_lang_function('EN', 'hours_before');
        $remove = $lang_obj->sbh_change_lang_function('EN', 'remove');
        $hours_after = $lang_obj->sbh_change_lang_function('EN', 'hours_after');
        $add = $lang_obj->sbh_change_lang_function('EN', 'add');
        $close = $lang_obj->sbh_change_lang_function('EN', 'close');
        $open = $lang_obj->sbh_change_lang_function('EN', 'open');
        $repeat_yearly = $lang_obj->sbh_change_lang_function('EN', 'repeat_yearly');
        $preview_list = $lang_obj->sbh_change_lang_function('EN', 'preview_list');
        $closed = $lang_obj->sbh_change_lang_function('EN', 'closed');
        $select_background_image = $lang_obj->sbh_change_lang_function('EN', 'select_background_image');
    }
}
?>
<style>
    @media screen and (max-width:1024px)
    {
        .custom_business_hour_plugin .holiday_list date-td {
            font-size: 9px;
            border-radius: 0;
            width:5px!important;
        }
        .custom_business_hour_plugin .date-td input {
            width: 52%;
        }
    }
</style>
<?php $shortcode = isset($_REQUEST['id']) ? 'stylish_business_hour id="' . $id_shortcode[3] . '"' : 'stylish_business_hour id=""'; ?>
<div class="custom_business_hour_plugin" abspath="<?php echo SBH_DIR; ?>" site_url="<?php echo SBH_URL; ?>">
    <h2>Business Hour Plugin</h2>
    <img src="<?php echo SBH_URL . 'assets/images/Stylish-Business-Hours-Logo1.png'; ?>" style="width: 300px;"/>
    <div class="custom_main_section">
        <form id="business_hour_form" class="business_hour_form" method="post" stylis_business_h="<?php
        if (isset($_GET['id'])):echo $_GET['id'];
        endif;
        ?>" ajax_url_data="<?php echo admin_url('admin-ajax.php'); ?>" enctype="multipart/form-data">
            <div class="shortcode-generater">
                <div class="shortcode-inner">
                    <p><input type="button" value="<?php echo $save; ?>" id="shortcode_generate" class="shortcode_generate_business_hour save"/></p>
                    <p><?php echo $your_shortcode; ?></p>
                    <p><span id="shortcode_business_hour">[<?php echo $shortcode; ?>]</span></p>
                    <p><?php echo $change_language; ?></p>
                    <p>
                        <select class="form-control" id="select_lang" name="select_lang">
                            <?php if (isset($_REQUEST['lang'])) { ?>
                                <option class="form-control" value="EN" <?php
                                if ($_REQUEST['lang'] == 'EN') {
                                    echo "selected";
                                }
                                ?> >EN</option>
                                <option class="form-control" value="SP" <?php
                                if ($_REQUEST['lang'] == 'SP') {
                                    echo "selected";
                                }
                                ?> >SP</option>
                                <option class="form-control" value="FR" <?php
                                if ($_REQUEST['lang'] == 'FR') {
                                    echo "selected";
                                }
                                ?> >FR</option>
                                <option class="form-control" value="DE" <?php
                                if ($_REQUEST['lang'] == 'DE') {
                                    echo "selected";
                                }
                                ?> >DE</option>
                                    <?php } else {
                                        ?>
                                <option class="form-control" value="EN" <?php
                                if ($datas->lang == 'EN') {
                                    echo "selected";
                                }
                                ?> >EN</option>
                                <option class="form-control" value="SP" <?php
                                if ($datas->lang == 'SP') {
                                    echo "selected";
                                }
                                ?> >SP</option>
                                <option class="form-control" value="FR" <?php
                                if ($datas->lang == 'FR') {
                                    echo "selected";
                                }
                                ?> >FR</option>
                                <option class="form-control" value="DE" <?php
                                if ($datas->lang == 'DE') {
                                    echo "selected";
                                }
                                ?> >DE</option>
                                        <?php
                                    }
                                    ?>
                        </select> 
                    </p>
                </div>    
            </div>
            <h2 class="title-plg"><?php echo $admin_setting; ?></h2>
            <table id="holidays_admin_setting">
                <?php
                if (empty($opt) || $opt != '1') {
                    echo '<p class="free_version">You are using the <span class="highlighted">Demo</span> version of the plugin. Click <span class="highlighted"><a href="#">here</a></span> to buy the pro version.</p>';
                }
                ?>
                <tbody>
                    <tr>
                        <th class="list_name"><?php echo $list_name; ?></th>
                        <td>
                            <input type="text" class="business_hour_name" id="business_hour_name" name="business_hour_name" placeholder="List name">
                        </td>
                        <td class="full_width"><button type="button" class="select-icon"><?php echo $select_icon; ?></button></td>
                        <td class="full_width">
                            <div class=icon-list style="display:none">
                                <input type="radio" name="icon-img" value="<?php echo SBH_URL . 'assets/images/sbh_icon.png'; ?>" <?php echo isset($selected_icon) ? 'checked' : '' ?> > <img src="<?php echo SBH_URL . 'assets/images/sbh_icon.png'; ?>" /><br>
                                <input type="radio" name="icon-img" value="<?php echo SBH_URL . 'assets/images/sbh_icon2.png'; ?>" <?php echo isset($selected_icon) ? 'checked' : '' ?> > <img src="<?php echo SBH_URL . 'assets/images/sbh_icon2.png'; ?>" /><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $style; ?></th>
                        <td>
                            <select class="style-drop" id="style_business">
                                <option value="style_business_1">Style 1</option>
<?php if (empty($opt) || $opt != '1') { ?>
                                    <option disabled="true" value="style_business_2">Style 2</option>
                                    <option disabled="true" value="style_business_3">Style 3</option>
                                    <option disabled="true" value="style_business_4">Style 4</option>
                                    <option disabled="true" value="style_business_5">Style 5</option>
                                    <option disabled="true" value="style_business_6">Style 6</option>
                                    <option disabled="true" value="style_business_7">Style 7</option>
                                    <option disabled="true" value="style_business_8">Style 8</option>
                                    <option disabled="true" value="style_business_9">Style 9</option>
<?php } else { ?>
                                    <option value="style_business_2">Style 2</option>
                                    <option value="style_business_3">Style 3</option>
                                    <option value="style_business_4">Style 4</option>
                                    <option value="style_business_5">Style 5</option>
                                    <option value="style_business_6">Style 6</option>
                                    <option value="style_business_7">Style 7</option>
                                    <option value="style_business_8">Style 8</option>
                                    <option value="style_business_9">Style 9</option>
<?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $font_size; ?></th>
                        <td>
                            <input class="bhp_fontsize font-size" value="14px">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $current_day_highlight_color; ?></th>
                        <td>
                            <input class="jscolor" value="#ACACAC" id="current_day_color" name="jscolor">
                        </td>
                    </tr>
                    <tr>
                        <th>Item/Icon Color</th>
                        <td>
                            <input class="jscolor" value="<?php echo $item_icon_color; ?>" id="item_icon_color" name="item_icon_color">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $holiday_highlight_color; ?></th>
                        <td>
                            <input class="jscolor" value="FF0000" id="jscolor_holiday_color" name="jscolor_holiday">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo $font_type; ?></th>
                        <td>
                            <select class="style-drop bhp_fonttype">
                                <?php
                                foreach ($font_list as $font_data):
                                    if ($font_data['font_name'] == 'Open Sans') {
                                        $select = 'selected="selected"';
                                        echo '<option value="' . $font_data['font_name'] . '" ' . $select . '>' . $font_data['font_name'] . '</option>';
                                    } else {
                                        echo '<option value="' . $font_data['font_name'] . '" >' . $font_data['font_name'] . '</option>';
                                    }

                                endforeach;
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="style_9_back_image_tr" style="display:none">
                        <th><?php echo $select_background_image; ?></th>
                        <td>
                            <input type="file" class="style_9_back_image" value="" id="file"  name="style_9_back_image">
                        </td>
                    </tr>
                </tbody>
            </table> 

            <!---mockup images--------->	
            <div class="container">
                <div class= "row">
                    <div class = "col-sm-6 business-img">
                        <div id="style_business_1" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-1.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_2" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-2.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_3" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-3.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_4" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-4.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_5" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-5.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_6" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-6.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_7" style="display:none"> 
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-7.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_8" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-8.png'; ?>" alt="some image" />
                        </div>
                        <div id="style_business_9" style="display:none">
                            <img src="<?php echo SBH_URL . 'assets/images/Style-list-9.png'; ?>" alt="some image" />
                        </div>
                    </div>
                </div>
            </div>
            <!---mockup images end--------->


            <h2 class="title-plg regular_hours"><?php echo $regular_business_hours; ?></h2>
            <table class="form-table business_time_data">
                <tbody>
                    <tr>
                        <th scope="row"><?php echo $select_time_zone; ?></th>
                        <td>
                            <select name="bs_custom_timezone" class="bs_custom_timezone">
                                <?php
                                foreach ($timezone as $key => $data) {
                                    echo '<option value=' . $key . '>' . $data . '</option>';
                                }
                                ?>
                            </select>	
                        </td>
                    </tr>
                    <tr class="time-bg"><th><?php echo $time_format; ?></th>
                        <td>
                            <select name="bs_custom_timeformat" class="bs_custom_timeformat style-drop1">
                                <option value="12">12 Hour</option>
                                <option value="24">24 Hour</option>
                            </select>
                    </tr>

                <div class="display_type">
                    <?php
                    if (empty($opt) || $opt != '1') {
                        $class = "free_version_sbh";
                    } else {
                        $class = "licensed_version_sbh";
                    }
                    ?>

                    <tr class="display_alldays mon_tr">
                        <th><?php echo $monday; ?></th>
                        <td class="<?php echo $class; ?>"><label class="switch">
                                <input type="checkbox" class="timing"  day="Monday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Monday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label>
                            <span class="status"><?php echo isset($days->Monday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>


                    <tr class="display_alldays tue_tr">
                        <th><?php echo $tuesday; ?></th>
                        <td class="<?php echo $class; ?>"><label class="switch">
                                <input type="checkbox" class="timing"  day="Tuesday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Tuesday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Tuesday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_alldays wed_tr">
                        <th><?php echo $wednesday; ?></th>
                        <td class="<?php echo $class; ?>"><label class="switch">
                                <input type="checkbox" class="timing"  day="Wednesday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Wednesday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Wednesday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_alldays thu_tr">
                        <th><?php echo $thursday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Thursday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Thursday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Thursday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_alldays fri_tr">
                        <th><?php echo $friday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Friday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Friday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Friday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_alldays sat_tr">
                        <th><?php echo $saturday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Saturday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Saturday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Saturday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_alldays sun_tr">
                        <th><?php echo $sunday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Sunday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Sunday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Sunday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>

                    <tr class="display_weekday mon_fri_tr">
                        <th><?php echo $monday; ?>&nbsp; - &nbsp;<?php echo $friday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Monday-Friday" value="0">
                                <span class="slider round"></span>
                                <?php
                                $mon_fri_obj = "Monday-Friday";
                                if ($days->$mon_fri_obj->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->$mon_fri_obj->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_weekday sat_tr">
                        <th><?php echo $saturday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Saturday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Saturday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Saturday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    <tr class="display_weekday sun_tr">
                        <th><?php echo $sunday; ?></th>
                        <td><label class="switch">
                                <input type="checkbox" class="timing"  day="Sunday" value="0">
                                <span class="slider round"></span>
                                <?php
                                if ($days->Sunday->status == 'open') {
                                    $status_day = $open;
                                } else {
                                    $status_day = $close;
                                }
                                ?>
                            </label><span class="status"><?php echo isset($days->Sunday->status) ? ucfirst($status_day) : $closed; ?></span></td>
                    </tr>
                    </tbody>
            </table>
            <h2  class="title-plg"><?php echo $holidays_and_special_hours; ?></h2>

            <table id="form-table" class="holidays_special_hours">
                <tbody>

                    <tr class="time-bg1">
                <div class="time-bg1"> <div class="same-11" > <?php echo $display; ?> <input name="hour_before" class="hour_before" type="text" value="<?php echo isset($datas->hour_before) ? $datas->hour_before : ''; ?>" > <?php echo $hours_before; ?></div >
                    <div class="same-11"><?php echo $remove; ?><input name="hour_after" class="hour_after" type="text" value="<?php echo isset($datas->hour_after) ? $datas->hour_after : ''; ?>" > <?php echo $hours_after; ?> </div ></div> 
                </tr>
                <tr>
                    Enter hours for days when this business has an irregular schedule
                </tr>

                <tr><td><input type="button" name="button" class="add_special_holiday" value="<?php echo $add ?>"></td></tr>

                </tbody>
            </table>

        </form>
        <input type="hidden" name="selected_lang" class="selected_lang" value="<?php echo $datas->lang; ?>"/>
        <input type="hidden" name="change_lang" class="change_lang" value="<?php echo $_REQUEST['lang']; ?>"/>
        <input type="hidden" name="selected_view" class="selected_view" value=""/>
        <h2 class="title-plg preview_frontend"><?php echo $preview_list; ?></h2>
        <div id="preview_data_design">
<?php echo do_shortcode('[' . $shortcode . ']'); ?>
        </div>
    </div>
    <input type="button" value="<?php echo $save; ?>" id="shortcode_generate" class="shortcode_generate_business_hour save">
</div>
</div>
<?php
require_once SBH_DIR . '/admin/functions/logo-footer.php';
?> 
<script>
    jQuery(document).ready(function () {
        jQuery('#select_lang').change(function () {
            var lang = jQuery(this).val();
            var url = "<?php echo $_SERVER['REQUEST_URI'] . '&lang=' ?>" + lang;
            window.location.href = url;
        });
    })
</script>