<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class Stylish_Business_Hour_Settings {

    public function __construct() {
        add_action('admin_init', array($this, 'admin_init'));
        add_action('admin_menu', array($this, 'admin_menu'), 90);
        $this->license_return = get_option('sbh_license_return');
    }

    function admin_init() {
        register_setting('stylishbh_options', 'sbh_license_key', array($this, 'process_key'));
    }

    function process_key($key) {
        if (isset($_REQUEST['activate'])) {
            $license_return = $this->activate($key);
        } else if (isset($_REQUEST['deactivate'])) {
            $license_return = $this->deactivate($key);
        }
        update_option('sbh_license_return', $license_return);
        return $key;
    }

    function option_page() {
        ob_start();
        include_once SBH_DIR . '/admin/functions/logo-header.php';

        $sbh_license_key = get_option('sbh_license_key');
        $icon_class = 'dashicons-no';
        $icon_style = 'color:red;';
        $opt = get_option('sbh_license_return');
        if (!empty($opt) && $opt == '1') {
            $icon_class = 'dashicons-yes';
            $icon_style = 'color:green;';
        }
        if (isset($_GET['settings-updated']) && !empty($opt)) {
            ?>
            <div id="message" class="updated">
                <p><strong><?php _e('Settings saved.') ?></strong></p>
            </div>
        <?php } ?>
        <div class="wrap"><?php screen_icon(); ?>
            <h2>Stylish Business Hours Settings</h2>
            <form action="options.php" method="post" id=stylishbh-admin-options-form"> 
        <?php settings_fields('stylishbh_options'); ?>
                <style type="text/css">
                    .dashicons-no::before,
                    .dashicons-yes::before {
                        font-size: 25px;
                    }
                </style>
                <label for="sbh_license_key">License Key: </label><br/>
                <input type="password" id="sbh_license_key" name="sbh_license_key" value="<?php echo $sbh_license_key; ?>" />
                <span class="<?php echo $icon_class; ?> dashicons-before" style="<?php echo $icon_style; ?>"></span><br/>
                <p>
                    <input type="submit" name="activate" value="Activate" class="button button-primary" />
                    <input type="submit" name="deactivate" value="Deactivate" class="button button-default" style="margin-left:30px;"/></p>
                    <?php
                    if ($this->license_return != "1") {
                        echo $this->license_return;
                    }
                    ?>
            </form>
        </div>
        <?php
        $html_1 = ob_get_clean();
        echo $html_1;
    }

    function include_license_settings() {
        $license_settings = SBH_DIR . '/license-settings.php';
        if (file_exists($license_settings)) {
            require_once $license_settings;
            return true;
        } else {
            return 'cannot find the license-settings.php file in folder ' . SBH_DIR;
        }
    }

    function update_opt($opt) {
        update_option('sbhlk_opt', $opt);
    }

    function activate($key) {
        $opt = get_option('sbhlk_opt');
        ob_start();
        if (!empty($key)) {
            $license_data = $this->get_license_data($key, 'slm_activate');
            if (isset($license_data->result)) {
                if ($license_data->result == 'success') { //Success was returned for the license activation
                    $opt = get_object_vars($license_data);
                    $this->update_opt($opt);
                    return true;
                }
                if ($license_data->result == 'error') {

                    $message = "Your license has reached the maximum amount of domains. Please note, this error message might appear by accident if you pressed the enter button twice, in this case you can just ignore this error message. If there's a green check-mark beside your serial that means your pro version has been activated. If you're moving domains, then just de-activate your license on your first domain before activating SBH on another domain.";
                    return '<p style="color:red;"> Error: ' . $message . '</p>';
                }
            } else {
                return $license_data;
            }
        } else {
            $this->update_opt('');
            return 'The license key is empty';
        }
    }

    function deactivate($key) {
        if (!empty($key)) {
            $license_data = $this->get_license_data($key, 'slm_deactivate');
            if (isset($license_data->result)) {
                if ($license_data->result == 'success') {//Success was returned for the license activation
                    $this->update_opt('');
                    return true;
                } else {
                    return '<p style="color:red;"> Error: ' . $license_data->message . '</p>';
                }
            } else {
                return $license_data;
            }
        } else {
            $this->update_opt('');
            return 'The license key is empty';
        }
    }

    function get_license_data($key, $action) {

        $include_license = $this->include_license_settings();
        if ($include_license !== true) {
            return $include_license;
        }
        // API query parameters
        $api_params = array(
            'slm_action' => $action,
            'secret_key' => SPL_SPECIAL_SECRET_KEY,
            'license_key' => $key,
            'registered_domain' => $_SERVER['SERVER_NAME'],
            'item_reference' => urlencode(SPL_ITEM_REFERENCE),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, SPL_LICENSE_SERVER_URL));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));
        // Check for error in the response
        if (is_wp_error($response)) {
            return "Unexpected Error! The query returned with an error.";
        }

        //var_dump($response);//uncomment it if you want to look at the full response
        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));
        return $license_data;
    }

    function admin_menu() {
        add_submenu_page('business_hour_listing', __('Setting', 'stylishpl'), __('Settings', 'stylishpl'), 'manage_options', 'stylish_business_hour_license_settings', array($this, 'option_page'));
    }
}
$stylish_business_hour_settings = new Stylish_Business_Hour_Settings();
?>